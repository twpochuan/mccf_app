import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'models.g.dart';

// **************************************************************************
// This is a data model template converted from the GraphQL API.
// Built_value will look into this template and generate the actually data model code
// **************************************************************************


abstract class Node {
  String get id;
}

abstract class Edge {
  String get cursor;

  Node get node;
}

abstract class Connection {
  BuiltList<Edge> get edges;

  BuiltList<Node> get nodes;

  PageInfo get pageInfo;

  int get count;
}

abstract class PageInfo implements Built<PageInfo, PageInfoBuilder> {
  static Serializer<PageInfo> get serializer => _$pageInfoSerializer;

  String get startCursor;

  String get endCursor;

  bool get hasNextPage;

  bool get hasPreviousPage;

  PageInfo._();

  factory PageInfo([updates(PageInfoBuilder b)]) = _$PageInfo;
}

abstract class Actor implements Node {
  ActorName get name;

}

abstract class ActorName {
  String get displayName;
}

abstract class Channel implements Node {
  String get name;

  DateTime get viewerLastReadAt;

  DateTime get createdAt;

  DateTime get updatedAt;
}

//abstract class ChannelMemberConnection implements Connection {
//  @override
//  BuiltList<ChannelMemberEdge> get edges;
//
//  @override
//  BuiltList<Actor> nodes;
//}
//
//abstract class ChannelMemberEdge implements Edge {
//  @override
//  Actor get node;
//
//  DateTime get createdAt;
//}

abstract class ChannelMessage implements Node {
  Channel get channel;

  Actor get author;

  String get body;

  ChannelMessageState get state;

  DateTime get createdAt;

  DateTime get updatedAt;
}

class ChannelMessageState extends EnumClass {
  static Serializer<ChannelMessageState> get serializer =>
      _$channelMessageStateSerializer;

  @BuiltValueField(wireName: 'NORMAL')
  static const ChannelMessageState normal = _$normal;
  @BuiltValueField(wireName: 'DELETED')
  static const ChannelMessageState deleted = _$deleted;

  const ChannelMessageState._(String name) : super(name);

  static BuiltSet<ChannelMessageState> get values => _$channelMessageStateValues;

  static ChannelMessageState valueOf(String name) => _$channelMessageStateValueOf(name);
}

class ChannelMessageType extends EnumClass {
  static Serializer<ChannelMessageType> get serializer =>
      _$channelMessageTypeSerializer;

  @BuiltValueField(wireName: 'TEXT')
  static const ChannelMessageType text = _$text;
  @BuiltValueField(wireName: 'IMAGE')
  static const ChannelMessageType minor = _$image;
  @BuiltValueField(wireName: 'GIF')
  static const ChannelMessageType gif = _$gif;
  @BuiltValueField(wireName: 'LINK')
  static const ChannelMessageType link = _$link;
  @BuiltValueField(wireName: 'EVENT')
  static const ChannelMessageType event = _$event;

  const ChannelMessageType._(String name) : super(name);

  static BuiltSet<ChannelMessageType> get values => _$channelMessageTypeValues;

  static ChannelMessageType valueOf(String name) => _$channelMessageTypeValueOf(name);
}

abstract class ChannelMessageConnection implements Connection {
  @override
  BuiltList<ChannelMessageEdge> get edges;

  @override
  BuiltList<ChannelMessage> get nodes;
}

abstract class ChannelMessageEdge implements Edge {
  @override
  ChannelMessage get node;
}

abstract class DateTimePeriod
    implements Built<DateTimePeriod, DateTimePeriodBuilder> {
  static Serializer<DateTimePeriod> get serializer =>
      _$dateTimePeriodSerializer;

  DateTime get start;

  DateTime get end;

  DateTimePeriod._();

  factory DateTimePeriod([updates(DateTimePeriodBuilder b)]) = _$DateTimePeriod;
}

abstract class User implements Built<User, UserBuilder>, Actor, Node {
  static Serializer<User> get serializer => _$userSerializer;

  @override
  UserName get name;

  UserGender get gender;

  School get school;

  String get headshotURL;

  DateTime get birth;

  UserRelationship get relationship;

  User._();

  factory User([updates(UserBuilder b)]) = _$User;
}

abstract class UserName implements Built<UserName, UserNameBuilder>, ActorName {
  static Serializer<UserName> get serializer => _$userNameSerializer;

  @override
  String get displayName;

  String get firstName;

  String get lastName;

  UserName._();

  factory UserName([updates(UserNameBuilder b)]) = _$UserName;
}

class UserGender extends EnumClass {
  static Serializer<UserGender> get serializer => _$userGenderSerializer;

  @BuiltValueField(wireName: 'MALE')
  static const UserGender male = _$male;
  @BuiltValueField(wireName: 'FEMALE')
  static const UserGender female = _$female;
  @BuiltValueField(wireName: 'OTHER')
  static const UserGender other = _$other;

  const UserGender._(String name) : super(name);

  static BuiltSet<UserGender> get values => _$userGenderValues;

  static UserGender valueOf(String name) => _$userGenderValueOf(name);
}

class UserRelationship extends EnumClass {
  static Serializer<UserRelationship> get serializer =>
      _$userRelationshipSerializer;

  @BuiltValueField(wireName: 'ME')
  static const UserRelationship me = _$me;
  @BuiltValueField(wireName: 'STRANGER')
  static const UserRelationship stranger = _$stranger;
  @BuiltValueField(wireName: 'ACQUAINTANCE')
  static const UserRelationship acquaintance = _$acquintance;
  @BuiltValueField(wireName: 'FRIEND')
  static const UserRelationship friend = _$friend;
  @BuiltValueField(wireName: 'BLOCKED')
  static const UserRelationship blocked = _$blocked;

  const UserRelationship._(String name) : super(name);

  static BuiltSet<UserRelationship> get values => _$userRelationshipValues;

  static UserRelationship valueOf(String name) => _$userRelationshipValueOf(name);
}

class UserEventState extends EnumClass {
  static Serializer<UserEventState> get serializer => _$userEventStateSerializer;

  @BuiltValueField(wireName: 'FOLLOWING')
  static const UserEventState following = _$userEventFollowing;
  @BuiltValueField(wireName: 'JOINING')
  static const UserEventState joining = _$userEventJoining;
  @BuiltValueField(wireName: 'FINISHED')
  static const UserEventState finished = _$userEventFinished;

  const UserEventState._(String name) : super(name);

  static BuiltSet<UserEventState> get values => _$userEventValues;

  static UserEventState valueOf(String name) => _$userEventValueOf(name);
}

abstract class UserEventConnection
    implements
        Built<UserEventConnection, UserEventConnectionBuilder>,
        Connection {
  static Serializer<UserEventConnection> get serializer =>
      _$userEventConnectionSerializer;

  @override
  BuiltList<UserEventEdge> get edges;

  @override
  BuiltList<Event> get nodes;

  UserEventConnection._();

  factory UserEventConnection([updates(UserEventConnectionBuilder b)]) =
      _$UserEventConnection;
}

abstract class UserEventEdge
    implements Built<UserEventEdge, UserEventEdgeBuilder>, Edge {
  static Serializer<UserEventEdge> get serializer => _$userEventEdgeSerializer;

  @override
  Event get node;

  DateTime get createdAt;

  UserEventState get state;

  UserEventEdge._();

  factory UserEventEdge([updates(UserEventEdgeBuilder b)]) = _$UserEventEdge;
}

abstract class UserFriendConnection
    implements
        Built<UserFriendConnection, UserFriendConnectionBuilder>,
        Connection {
  static Serializer<UserFriendConnection> get serializer =>
      _$userFriendConnectionSerializer;

  @override
  BuiltList<UserFriendEdge> get edges;

  @override
  BuiltList<User> get nodes;

  UserFriendConnection._();

  factory UserFriendConnection([updates(UserFriendConnectionBuilder b)]) =
      _$UserFriendConnection;
}

abstract class UserFieldConnection
    implements
        Built<UserFieldConnection, UserFieldConnectionBuilder>,
        Connection {
  static Serializer<UserFieldConnection> get serializer =>
      _$userFieldConnectionSerializer;

  @override
  BuiltList<UserFieldEdge> get edges;

  @override
  BuiltList<SchoolField> get nodes;

  UserFieldConnection._();

  factory UserFieldConnection([updates(UserFieldConnectionBuilder b)]) =
      _$UserFieldConnection;
}

abstract class UserFriendEdge
    implements Built<UserFriendEdge, UserFriendEdgeBuilder>, Edge {
  static Serializer<UserFriendEdge> get serializer =>
      _$userFriendEdgeSerializer;

  @override
  User get node;

  DateTime get createdAt;

  UserFriendEdge._();

  factory UserFriendEdge([updates(UserFriendEdgeBuilder b)]) = _$UserFriendEdge;
}

abstract class UserFieldEdge
    implements Built<UserFieldEdge, UserFieldEdgeBuilder>, Edge {
  static Serializer<UserFieldEdge> get serializer => _$userFieldEdgeSerializer;

  @override
  SchoolField get node;

  UserFieldState get state;

  UserFieldEdge._();

  factory UserFieldEdge([updates(UserFieldEdgeBuilder b)]) = _$UserFieldEdge;
}

class UserFieldState extends EnumClass {
  static Serializer<UserFieldState> get serializer => _$userFieldStateSerializer;

  @BuiltValueField(wireName: 'MAJOR')
  static const UserFieldState major = _$major;
  @BuiltValueField(wireName: 'MINOR')
  static const UserFieldState minor = _$minor;
  @BuiltValueField(wireName: 'RESEARCH')
  static const UserFieldState research = _$research;

  const UserFieldState._(String name) : super(name);

  static BuiltSet<UserFieldState> get values => _$userFieldStateValues;

  static UserFieldState valueOf(String name) => _$userFieldStateValueOf(name);
}

abstract class School implements Built<School, SchoolBuilder>, Node {
  static Serializer<School> get serializer => _$schoolSerializer;

  String get name;

  School._();

  factory School([updates(SchoolBuilder b)]) = _$School;
}

abstract class SchoolField
    implements Built<SchoolField, SchoolFieldBuilder>, Node {
  static Serializer<SchoolField> get serializer => _$schoolFieldSerializer;

  School get school;

  String get name;

  BuiltList<StudyField> get fields;

  SchoolField._();

  factory SchoolField([updates(SchoolFieldBuilder b)]) = _$SchoolField;
}

abstract class SchoolFieldConnection
    implements
        Built<SchoolFieldConnection, SchoolFieldConnectionBuilder>,
        Connection {
  static Serializer<SchoolFieldConnection> get serializer =>
      _$schoolFieldConnectionSerializer;

  @override
  BuiltList<SchoolFieldEdge> get edges;

  @override
  BuiltList<SchoolField> get nodes;

  SchoolFieldConnection._();

  factory SchoolFieldConnection([updates(SchoolFieldConnectionBuilder b)]) =
      _$SchoolFieldConnection;
}

abstract class SchoolFieldEdge
    implements Built<SchoolFieldEdge, SchoolFieldEdgeBuilder>, Edge {
  static Serializer<SchoolFieldEdge> get serializer =>
      _$schoolFieldEdgeSerializer;

  @override
  SchoolField get node;

  SchoolFieldEdge._();

  factory SchoolFieldEdge([updates(SchoolFieldEdgeBuilder b)]) =
      _$SchoolFieldEdge;
}

class StudyField extends EnumClass {
  static Serializer<StudyField> get serializer => _$studyFieldSerializer;

  @BuiltValueField(wireName: 'ARTS')
  static const StudyField arts = _$arts;
  @BuiltValueField(wireName: 'HISTORY')
  static const StudyField history = _$history;
  @BuiltValueField(wireName: 'LANGUAGE_AND_LITERATURE')
  static const StudyField languageAndLiterature = _$languageAndLiterature;
  @BuiltValueField(wireName: 'PHILOSOPHY')
  static const StudyField philosophy = _$philosophy;
  @BuiltValueField(wireName: 'THEOLOGY')
  static const StudyField theology = _$theology;
  @BuiltValueField(wireName: 'ANTHROPOLOGY')
  static const StudyField anthropology = _$anthropology;
  @BuiltValueField(wireName: 'ECONOMICS')
  static const StudyField economics = _$economics;
  @BuiltValueField(wireName: 'HUMAN_GEOGRAPHY')
  static const StudyField humanGeography = _$humanGeography;
  @BuiltValueField(wireName: 'LAW')
  static const StudyField law = _$law;
  @BuiltValueField(wireName: 'POLITICAL_SCEINCE')
  static const StudyField politicalSceince = _$politicalSceince;
  @BuiltValueField(wireName: 'PSYCHOLOGY')
  static const StudyField psychology = _$psychology;
  @BuiltValueField(wireName: 'SOCIOLOGY')
  static const StudyField sociology = _$sociology;
  @BuiltValueField(wireName: 'BIOLOGY')
  static const StudyField biology = _$biology;
  @BuiltValueField(wireName: 'CHEMISTRY')
  static const StudyField chemistry = _$chemistry;
  @BuiltValueField(wireName: 'EARTH_SCIENCE')
  static const StudyField earthScience = _$earthScience;
  @BuiltValueField(wireName: 'SPACE_SCIENCE')
  static const StudyField spaceScience = _$spaceScience;
  @BuiltValueField(wireName: 'PHYSICS')
  static const StudyField physics = _$physics;
  @BuiltValueField(wireName: 'COMPUTER_SCIENCE')
  static const StudyField computerScience = _$computerScience;
  @BuiltValueField(wireName: 'MATHEMATICS')
  static const StudyField mathematics = _$mathematics;
  @BuiltValueField(wireName: 'STATISTICS')
  static const StudyField statistics = _$statistics;
  @BuiltValueField(wireName: 'EGINEERING_AND_TECHNOLOGY')
  static const StudyField engineeringAndTechnology = _$engineeringAndTechnology;
  @BuiltValueField(wireName: 'MEDICINE_AND_HEALTH')
  static const StudyField medicineAndHealth = _$medicineAndHealth;

  const StudyField._(String name) : super(name);

  static BuiltSet<StudyField> get values => _$studyFieldValues;

  static StudyField valueOf(String name) => _$studyFieldValueOf(name);
}

abstract class Event implements Built<Event, EventBuilder>, Node {
  static Serializer<Event> get serializer => _$eventSerializer;

  String get name;

  String get description;

  DateTimePeriod get hostDate;

  EventChannel get channel;

  EventState get state;

  UserEventState get viewerState;

  bool get viewerCanJoin;

  bool get viewerCanFollow;

  Event._();

  factory Event([updates(EventBuilder b)]) = _$Event;
}

class EventState extends EnumClass {
  static Serializer<EventState> get serializer =>
      _$eventStateSerializer;

  @BuiltValueField(wireName: 'UNAVAILABLE')
  static const EventState unavailable = _$eventUnavailable;
  @BuiltValueField(wireName: 'AVAILABLE')
  static const EventState available = _$eventAvailable;
  @BuiltValueField(wireName: 'CLOSED')
  static const EventState closed = _$eventClosed;
  @BuiltValueField(wireName: 'FINISHED')
  static const EventState finished = _$eventFinished;

  const EventState._(String name) : super(name);

  static BuiltSet<EventState> get values => _$eventStateValues;

  static EventState valueOf(String name) => _$eventStateValueOf(name);
}

abstract class EventHostConnection
    implements
        Built<EventHostConnection, EventHostConnectionBuilder>,
        Connection {
  static Serializer<EventHostConnection> get serializer =>
      _$eventHostConnectionSerializer;

  @override
  BuiltList<EventHostEdge> get edges;

  @override
  BuiltList<Actor> get nodes;

  EventHostConnection._();

  factory EventHostConnection([updates(EventHostConnectionBuilder b)]) =
      _$EventHostConnection;
}

abstract class EventHostEdge
    implements Built<EventHostEdge, EventHostEdgeBuilder>, Edge {
  static Serializer<EventHostEdge> get serializer => _$eventHostEdgeSerializer;

  @override
  Actor get node;

  EventHostEdge._();

  factory EventHostEdge([updates(EventHostEdgeBuilder b)]) = _$EventHostEdge;
}

abstract class EventFriendConnection
    implements
        Built<EventFriendConnection, EventFriendConnectionBuilder>,
        Connection {
  static Serializer<EventFriendConnection> get serializer =>
      _$eventFriendConnectionSerializer;

  @override
  BuiltList<EventFriendEdge> get edges;

  @override
  BuiltList<User> get nodes;

  EventFriendConnection._();

  factory EventFriendConnection([updates(EventFriendConnectionBuilder b)]) =
      _$EventFriendConnection;
}

abstract class EventFriendEdge
    implements Built<EventFriendEdge, EventFriendEdgeBuilder>, Edge {
  static Serializer<EventFriendEdge> get serializer =>
      _$eventFriendEdgeSerializer;

  @override
  User get node;

  UserEventState get state;

  EventFriendEdge._();

  factory EventFriendEdge([updates(EventFriendEdgeBuilder b)]) =
      _$EventFriendEdge;
}

abstract class EventChannel
    implements Built<EventChannel, EventChannelBuilder>, Channel {
  static Serializer<EventChannel> get serializer => _$eventChannelSerializer;

  String get coverUrl;

  bool get viewerCanAdminister;

  bool get viewerCanChat;

  bool get viewerIsMember;

  EventChannel._();

  factory EventChannel([updates(EventChannelBuilder b)]) = _$EventChannel;
}

abstract class EventChannelMemberConnection
    implements
        Built<EventChannelMemberConnection,
            EventChannelMemberConnectionBuilder>,
        Connection {
  static Serializer<EventChannelMemberConnection> get serializer =>
      _$eventChannelMemberConnectionSerializer;

  @override
  BuiltList<EventChannelMemberEdge> get edges;
  @override
  BuiltList<Actor> get nodes;

  EventChannelMemberConnection._();

  factory EventChannelMemberConnection(
          [updates(EventChannelMemberConnectionBuilder b)]) =
      _$EventChannelMemberConnection;
}

abstract class EventChannelMemberEdge
    implements
        Built<EventChannelMemberEdge, EventChannelMemberEdgeBuilder>,
        Edge {
  static Serializer<EventChannelMemberEdge> get serializer =>
      _$eventChannelMemberEdgeSerializer;

  @override
  Actor get node;
  UserEventState get state;
  DateTime get createdAt;


  EventChannelMemberEdge._();

  factory EventChannelMemberEdge([updates(EventChannelMemberEdgeBuilder b)]) =
      _$EventChannelMemberEdge;
}

abstract class EventChannelMessage
    implements
        Built<EventChannelMessage, EventChannelMessageBuilder>,
        ChannelMessage {
  static Serializer<EventChannelMessage> get serializer =>
      _$eventChannelMessageSerializer;

  EventChannelMessage._();

  factory EventChannelMessage([updates(EventChannelMessageBuilder b)]) =
      _$EventChannelMessage;
}

abstract class EventChannelMessageConnection
    implements
        Built<EventChannelMessageConnection,
            EventChannelMessageConnectionBuilder>,
        ChannelMessageConnection {
  static Serializer<EventChannelMessageConnection> get serializer =>
      _$eventChannelMessageConnectionSerializer;

  BuiltList<EventChannelMessageEdge> get edges;

  BuiltList<EventChannelMessage> get nodes;

  EventChannelMessageConnection._();

  factory EventChannelMessageConnection(
          [updates(EventChannelMessageConnectionBuilder b)]) =
      _$EventChannelMessageConnection;
}

abstract class EventChannelMessageEdge
    implements
        Built<EventChannelMessageEdge, EventChannelMessageEdgeBuilder>,
        ChannelMessageEdge {
  static Serializer<EventChannelMessageEdge> get serializer =>
      _$eventChannelMessageEdgeSerializer;

  @override
  EventChannelMessage get node;

  EventChannelMessageEdge._();

  factory EventChannelMessageEdge([updates(EventChannelMessageEdgeBuilder b)]) =
      _$EventChannelMessageEdge;
}

abstract class PersonalChannel
    implements Built<PersonalChannel, PersonalChannelBuilder>, Channel {
  static Serializer<PersonalChannel> get serializer =>
      _$personalChannelSerializer;

  PersonalChannel._();

  factory PersonalChannel([updates(PersonalChannelBuilder b)]) =
      _$PersonalChannel;
}

abstract class PersonalChannelConnection
    implements
        Built<PersonalChannelConnection, PersonalChannelConnectionBuilder>,
        Connection {
  static Serializer<PersonalChannelConnection> get serializer =>
      _$personalChannelConnectionSerializer;

  @override
  BuiltList<PersonalChannelEdge> get edges;

  @override
  BuiltList<PersonalChannel> get nodes;

  PersonalChannelConnection._();

  factory PersonalChannelConnection(
          [updates(PersonalChannelConnectionBuilder b)]) =
      _$PersonalChannelConnection;
}

abstract class PersonalChannelEdge
    implements Built<PersonalChannelEdge, PersonalChannelEdgeBuilder>, Edge {
  static Serializer<PersonalChannelEdge> get serializer =>
      _$personalChannelEdgeSerializer;

  @override
  PersonalChannel get node;

  PersonalChannelEdge._();

  factory PersonalChannelEdge([updates(PersonalChannelEdgeBuilder b)]) =
      _$PersonalChannelEdge;
}

abstract class PersonalChannelMessage
    implements
        Built<PersonalChannelMessage, PersonalChannelMessageBuilder>,
        Node,
        ChannelMessage {
  static Serializer<PersonalChannelMessage> get serializer =>
      _$personalChannelMessageSerializer;

  PersonalChannelMessage._();

  factory PersonalChannelMessage([updates(PersonalChannelMessageBuilder b)]) =
      _$PersonalChannelMessage;
}

abstract class PersonalChannelMessageConnection
    implements
        Built<PersonalChannelMessageConnection,
            PersonalChannelMessageConnectionBuilder>,
        ChannelMessageEdge {
  static Serializer<PersonalChannelMessageConnection> get serializer =>
      _$personalChannelMessageConnectionSerializer;

  @override
  BuiltList<PersonalChannelMessageEdge> get edges;

  @override
  BuiltList<PersonalChannelMessage> get nodes;

  PersonalChannelMessageConnection._();

  factory PersonalChannelMessageConnection(
          [updates(PersonalChannelMessageConnectionBuilder b)]) =
      _$PersonalChannelMessageConnection;
}

abstract class PersonalChannelMessageEdge
    implements
        Built<PersonalChannelMessageEdge, PersonalChannelMessageEdgeBuilder>,
        ChannelMessageEdge {
  static Serializer<PersonalChannelMessageEdge> get serializer =>
      _$personalChannelMessageEdgeSerializer;

  @override
  PersonalChannelMessage get node;

  PersonalChannelMessageEdge._();

  factory PersonalChannelMessageEdge(
          [updates(PersonalChannelMessageEdgeBuilder b)]) =
      _$PersonalChannelMessageEdge;
}

abstract class PersonalChannelMemberConnection
    implements
        Built<PersonalChannelMemberConnection,
            PersonalChannelMemberConnectionBuilder>,
        Connection {
  static Serializer<PersonalChannelMemberConnection> get serializer =>
      _$personalChannelMemberConnectionSerializer;

  @override
  BuiltList<PersonalChannelMemberEdge> get edges;
  @override
  BuiltList<Actor> get nodes;


  PersonalChannelMemberConnection._();

  factory PersonalChannelMemberConnection(
          [updates(PersonalChannelMemberConnectionBuilder b)]) =
      _$PersonalChannelMemberConnection;
}

abstract class PersonalChannelMemberEdge
    implements
        Built<PersonalChannelMemberEdge, PersonalChannelMemberEdgeBuilder>,
        Edge {
  static Serializer<PersonalChannelMemberEdge> get serializer =>
      _$personalChannelMemberEdgeSerializer;

  @override
  Actor get node;
  DateTime get createdAt;

  PersonalChannelMemberEdge._();

  factory PersonalChannelMemberEdge(
          [updates(PersonalChannelMemberEdgeBuilder b)]) =
      _$PersonalChannelMemberEdge;
}
