import 'package:built_collection/built_collection.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/serializer.dart';
import 'package:mccf/src/models/models.dart';

part 'serializers.g.dart';

@SerializersFor(const [
  User,
  UserName,
  UserGender,
  UserRelationship,
  UserFriendConnection,
  UserFriendEdge,
  UserEventConnection,
  UserEventEdge,
  UserEventState,
  UserFieldConnection,
  UserFieldEdge,
  School,
  SchoolField,
  SchoolFieldConnection,
  SchoolFieldEdge,
  StudyField,
  DateTimePeriod,
  ChannelMessageState,
  ChannelMessageType,
  Event,
  EventState,
  EventFriendConnection,
  EventFriendEdge,
  EventHostConnection,
  EventHostEdge,
  EventChannel,
  EventChannelMemberConnection,
  EventChannelMemberEdge,
  EventChannelMessage,
  EventChannelMessageConnection,
  EventChannelMemberEdge,
  PersonalChannel,
  PersonalChannelConnection,
  PersonalChannelEdge,
  PersonalChannelMemberConnection,
  PersonalChannelMemberEdge,
  PersonalChannelMessage,
  PersonalChannelMessageConnection,
  PersonalChannelMessageEdge,

])
final Serializers serializers = _$serializers;