// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line
// ignore_for_file: annotate_overrides
// ignore_for_file: avoid_annotating_with_dynamic
// ignore_for_file: avoid_catches_without_on_clauses
// ignore_for_file: avoid_returning_this
// ignore_for_file: lines_longer_than_80_chars
// ignore_for_file: omit_local_variable_types
// ignore_for_file: prefer_expression_function_bodies
// ignore_for_file: sort_constructors_first
// ignore_for_file: unnecessary_const
// ignore_for_file: unnecessary_new
// ignore_for_file: test_types_in_equals

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(ChannelMessageState.serializer)
      ..add(ChannelMessageType.serializer)
      ..add(DateTimePeriod.serializer)
      ..add(Event.serializer)
      ..add(EventChannel.serializer)
      ..add(EventChannelMemberConnection.serializer)
      ..add(EventChannelMemberEdge.serializer)
      ..add(EventChannelMessage.serializer)
      ..add(EventChannelMessageConnection.serializer)
      ..add(EventChannelMessageEdge.serializer)
      ..add(EventFriendConnection.serializer)
      ..add(EventFriendEdge.serializer)
      ..add(EventHostConnection.serializer)
      ..add(EventHostEdge.serializer)
      ..add(EventState.serializer)
      ..add(PageInfo.serializer)
      ..add(PersonalChannel.serializer)
      ..add(PersonalChannelConnection.serializer)
      ..add(PersonalChannelEdge.serializer)
      ..add(PersonalChannelMemberConnection.serializer)
      ..add(PersonalChannelMemberEdge.serializer)
      ..add(PersonalChannelMessage.serializer)
      ..add(PersonalChannelMessageConnection.serializer)
      ..add(PersonalChannelMessageEdge.serializer)
      ..add(School.serializer)
      ..add(SchoolField.serializer)
      ..add(SchoolFieldConnection.serializer)
      ..add(SchoolFieldEdge.serializer)
      ..add(StudyField.serializer)
      ..add(User.serializer)
      ..add(UserEventConnection.serializer)
      ..add(UserEventEdge.serializer)
      ..add(UserEventState.serializer)
      ..add(UserFieldConnection.serializer)
      ..add(UserFieldEdge.serializer)
      ..add(UserFieldState.serializer)
      ..add(UserFriendConnection.serializer)
      ..add(UserFriendEdge.serializer)
      ..add(UserGender.serializer)
      ..add(UserName.serializer)
      ..add(UserRelationship.serializer)
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(EventChannelMemberEdge)]),
          () => new ListBuilder<EventChannelMemberEdge>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Actor)]),
          () => new ListBuilder<Actor>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(EventChannelMessageEdge)]),
          () => new ListBuilder<EventChannelMessageEdge>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(EventChannelMessage)]),
          () => new ListBuilder<EventChannelMessage>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(EventFriendEdge)]),
          () => new ListBuilder<EventFriendEdge>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(User)]),
          () => new ListBuilder<User>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(EventHostEdge)]),
          () => new ListBuilder<EventHostEdge>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Actor)]),
          () => new ListBuilder<Actor>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(PersonalChannelEdge)]),
          () => new ListBuilder<PersonalChannelEdge>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(PersonalChannel)]),
          () => new ListBuilder<PersonalChannel>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(PersonalChannelMemberEdge)]),
          () => new ListBuilder<PersonalChannelMemberEdge>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Actor)]),
          () => new ListBuilder<Actor>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(PersonalChannelMessageEdge)]),
          () => new ListBuilder<PersonalChannelMessageEdge>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(PersonalChannelMessage)]),
          () => new ListBuilder<PersonalChannelMessage>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SchoolFieldEdge)]),
          () => new ListBuilder<SchoolFieldEdge>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SchoolField)]),
          () => new ListBuilder<SchoolField>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(StudyField)]),
          () => new ListBuilder<StudyField>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(UserEventEdge)]),
          () => new ListBuilder<UserEventEdge>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Event)]),
          () => new ListBuilder<Event>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(UserFieldEdge)]),
          () => new ListBuilder<UserFieldEdge>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SchoolField)]),
          () => new ListBuilder<SchoolField>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(UserFriendEdge)]),
          () => new ListBuilder<UserFriendEdge>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(User)]),
          () => new ListBuilder<User>()))
    .build();
