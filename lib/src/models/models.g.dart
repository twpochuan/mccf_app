// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'models.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line
// ignore_for_file: annotate_overrides
// ignore_for_file: avoid_annotating_with_dynamic
// ignore_for_file: avoid_catches_without_on_clauses
// ignore_for_file: avoid_returning_this
// ignore_for_file: lines_longer_than_80_chars
// ignore_for_file: omit_local_variable_types
// ignore_for_file: prefer_expression_function_bodies
// ignore_for_file: sort_constructors_first
// ignore_for_file: unnecessary_const
// ignore_for_file: unnecessary_new
// ignore_for_file: test_types_in_equals

const ChannelMessageState _$normal = const ChannelMessageState._('normal');
const ChannelMessageState _$deleted = const ChannelMessageState._('deleted');

ChannelMessageState _$channelMessageStateValueOf(String name) {
  switch (name) {
    case 'normal':
      return _$normal;
    case 'deleted':
      return _$deleted;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<ChannelMessageState> _$channelMessageStateValues =
    new BuiltSet<ChannelMessageState>(const <ChannelMessageState>[
  _$normal,
  _$deleted,
]);

const ChannelMessageType _$text = const ChannelMessageType._('text');
const ChannelMessageType _$image = const ChannelMessageType._('minor');
const ChannelMessageType _$gif = const ChannelMessageType._('gif');
const ChannelMessageType _$link = const ChannelMessageType._('link');
const ChannelMessageType _$event = const ChannelMessageType._('event');

ChannelMessageType _$channelMessageTypeValueOf(String name) {
  switch (name) {
    case 'text':
      return _$text;
    case 'minor':
      return _$image;
    case 'gif':
      return _$gif;
    case 'link':
      return _$link;
    case 'event':
      return _$event;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<ChannelMessageType> _$channelMessageTypeValues =
    new BuiltSet<ChannelMessageType>(const <ChannelMessageType>[
  _$text,
  _$image,
  _$gif,
  _$link,
  _$event,
]);

const UserGender _$male = const UserGender._('male');
const UserGender _$female = const UserGender._('female');
const UserGender _$other = const UserGender._('other');

UserGender _$userGenderValueOf(String name) {
  switch (name) {
    case 'male':
      return _$male;
    case 'female':
      return _$female;
    case 'other':
      return _$other;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<UserGender> _$userGenderValues =
    new BuiltSet<UserGender>(const <UserGender>[
  _$male,
  _$female,
  _$other,
]);

const UserRelationship _$me = const UserRelationship._('me');
const UserRelationship _$stranger = const UserRelationship._('stranger');
const UserRelationship _$acquintance = const UserRelationship._('acquaintance');
const UserRelationship _$friend = const UserRelationship._('friend');
const UserRelationship _$blocked = const UserRelationship._('blocked');

UserRelationship _$userRelationshipValueOf(String name) {
  switch (name) {
    case 'me':
      return _$me;
    case 'stranger':
      return _$stranger;
    case 'acquaintance':
      return _$acquintance;
    case 'friend':
      return _$friend;
    case 'blocked':
      return _$blocked;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<UserRelationship> _$userRelationshipValues =
    new BuiltSet<UserRelationship>(const <UserRelationship>[
  _$me,
  _$stranger,
  _$acquintance,
  _$friend,
  _$blocked,
]);

const UserEventState _$userEventFollowing = const UserEventState._('following');
const UserEventState _$userEventJoining = const UserEventState._('joining');
const UserEventState _$userEventFinished = const UserEventState._('finished');

UserEventState _$userEventValueOf(String name) {
  switch (name) {
    case 'following':
      return _$userEventFollowing;
    case 'joining':
      return _$userEventJoining;
    case 'finished':
      return _$userEventFinished;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<UserEventState> _$userEventValues =
    new BuiltSet<UserEventState>(const <UserEventState>[
  _$userEventFollowing,
  _$userEventJoining,
  _$userEventFinished,
]);

const UserFieldState _$major = const UserFieldState._('major');
const UserFieldState _$minor = const UserFieldState._('minor');
const UserFieldState _$research = const UserFieldState._('research');

UserFieldState _$userFieldStateValueOf(String name) {
  switch (name) {
    case 'major':
      return _$major;
    case 'minor':
      return _$minor;
    case 'research':
      return _$research;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<UserFieldState> _$userFieldStateValues =
    new BuiltSet<UserFieldState>(const <UserFieldState>[
  _$major,
  _$minor,
  _$research,
]);

const StudyField _$arts = const StudyField._('arts');
const StudyField _$history = const StudyField._('history');
const StudyField _$languageAndLiterature =
    const StudyField._('languageAndLiterature');
const StudyField _$philosophy = const StudyField._('philosophy');
const StudyField _$theology = const StudyField._('theology');
const StudyField _$anthropology = const StudyField._('anthropology');
const StudyField _$economics = const StudyField._('economics');
const StudyField _$humanGeography = const StudyField._('humanGeography');
const StudyField _$law = const StudyField._('law');
const StudyField _$politicalSceince = const StudyField._('politicalSceince');
const StudyField _$psychology = const StudyField._('psychology');
const StudyField _$sociology = const StudyField._('sociology');
const StudyField _$biology = const StudyField._('biology');
const StudyField _$chemistry = const StudyField._('chemistry');
const StudyField _$earthScience = const StudyField._('earthScience');
const StudyField _$spaceScience = const StudyField._('spaceScience');
const StudyField _$physics = const StudyField._('physics');
const StudyField _$computerScience = const StudyField._('computerScience');
const StudyField _$mathematics = const StudyField._('mathematics');
const StudyField _$statistics = const StudyField._('statistics');
const StudyField _$engineeringAndTechnology =
    const StudyField._('engineeringAndTechnology');
const StudyField _$medicineAndHealth = const StudyField._('medicineAndHealth');

StudyField _$studyFieldValueOf(String name) {
  switch (name) {
    case 'arts':
      return _$arts;
    case 'history':
      return _$history;
    case 'languageAndLiterature':
      return _$languageAndLiterature;
    case 'philosophy':
      return _$philosophy;
    case 'theology':
      return _$theology;
    case 'anthropology':
      return _$anthropology;
    case 'economics':
      return _$economics;
    case 'humanGeography':
      return _$humanGeography;
    case 'law':
      return _$law;
    case 'politicalSceince':
      return _$politicalSceince;
    case 'psychology':
      return _$psychology;
    case 'sociology':
      return _$sociology;
    case 'biology':
      return _$biology;
    case 'chemistry':
      return _$chemistry;
    case 'earthScience':
      return _$earthScience;
    case 'spaceScience':
      return _$spaceScience;
    case 'physics':
      return _$physics;
    case 'computerScience':
      return _$computerScience;
    case 'mathematics':
      return _$mathematics;
    case 'statistics':
      return _$statistics;
    case 'engineeringAndTechnology':
      return _$engineeringAndTechnology;
    case 'medicineAndHealth':
      return _$medicineAndHealth;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<StudyField> _$studyFieldValues =
    new BuiltSet<StudyField>(const <StudyField>[
  _$arts,
  _$history,
  _$languageAndLiterature,
  _$philosophy,
  _$theology,
  _$anthropology,
  _$economics,
  _$humanGeography,
  _$law,
  _$politicalSceince,
  _$psychology,
  _$sociology,
  _$biology,
  _$chemistry,
  _$earthScience,
  _$spaceScience,
  _$physics,
  _$computerScience,
  _$mathematics,
  _$statistics,
  _$engineeringAndTechnology,
  _$medicineAndHealth,
]);

const EventState _$eventUnavailable = const EventState._('unavailable');
const EventState _$eventAvailable = const EventState._('available');
const EventState _$eventClosed = const EventState._('closed');
const EventState _$eventFinished = const EventState._('finished');

EventState _$eventStateValueOf(String name) {
  switch (name) {
    case 'unavailable':
      return _$eventUnavailable;
    case 'available':
      return _$eventAvailable;
    case 'closed':
      return _$eventClosed;
    case 'finished':
      return _$eventFinished;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<EventState> _$eventStateValues =
    new BuiltSet<EventState>(const <EventState>[
  _$eventUnavailable,
  _$eventAvailable,
  _$eventClosed,
  _$eventFinished,
]);

Serializer<PageInfo> _$pageInfoSerializer = new _$PageInfoSerializer();
Serializer<ChannelMessageState> _$channelMessageStateSerializer =
    new _$ChannelMessageStateSerializer();
Serializer<ChannelMessageType> _$channelMessageTypeSerializer =
    new _$ChannelMessageTypeSerializer();
Serializer<DateTimePeriod> _$dateTimePeriodSerializer =
    new _$DateTimePeriodSerializer();
Serializer<User> _$userSerializer = new _$UserSerializer();
Serializer<UserName> _$userNameSerializer = new _$UserNameSerializer();
Serializer<UserGender> _$userGenderSerializer = new _$UserGenderSerializer();
Serializer<UserRelationship> _$userRelationshipSerializer =
    new _$UserRelationshipSerializer();
Serializer<UserEventState> _$userEventStateSerializer =
    new _$UserEventStateSerializer();
Serializer<UserEventConnection> _$userEventConnectionSerializer =
    new _$UserEventConnectionSerializer();
Serializer<UserEventEdge> _$userEventEdgeSerializer =
    new _$UserEventEdgeSerializer();
Serializer<UserFriendConnection> _$userFriendConnectionSerializer =
    new _$UserFriendConnectionSerializer();
Serializer<UserFieldConnection> _$userFieldConnectionSerializer =
    new _$UserFieldConnectionSerializer();
Serializer<UserFriendEdge> _$userFriendEdgeSerializer =
    new _$UserFriendEdgeSerializer();
Serializer<UserFieldEdge> _$userFieldEdgeSerializer =
    new _$UserFieldEdgeSerializer();
Serializer<UserFieldState> _$userFieldStateSerializer =
    new _$UserFieldStateSerializer();
Serializer<School> _$schoolSerializer = new _$SchoolSerializer();
Serializer<SchoolField> _$schoolFieldSerializer = new _$SchoolFieldSerializer();
Serializer<SchoolFieldConnection> _$schoolFieldConnectionSerializer =
    new _$SchoolFieldConnectionSerializer();
Serializer<SchoolFieldEdge> _$schoolFieldEdgeSerializer =
    new _$SchoolFieldEdgeSerializer();
Serializer<StudyField> _$studyFieldSerializer = new _$StudyFieldSerializer();
Serializer<Event> _$eventSerializer = new _$EventSerializer();
Serializer<EventState> _$eventStateSerializer = new _$EventStateSerializer();
Serializer<EventHostConnection> _$eventHostConnectionSerializer =
    new _$EventHostConnectionSerializer();
Serializer<EventHostEdge> _$eventHostEdgeSerializer =
    new _$EventHostEdgeSerializer();
Serializer<EventFriendConnection> _$eventFriendConnectionSerializer =
    new _$EventFriendConnectionSerializer();
Serializer<EventFriendEdge> _$eventFriendEdgeSerializer =
    new _$EventFriendEdgeSerializer();
Serializer<EventChannel> _$eventChannelSerializer =
    new _$EventChannelSerializer();
Serializer<EventChannelMemberConnection>
    _$eventChannelMemberConnectionSerializer =
    new _$EventChannelMemberConnectionSerializer();
Serializer<EventChannelMemberEdge> _$eventChannelMemberEdgeSerializer =
    new _$EventChannelMemberEdgeSerializer();
Serializer<EventChannelMessage> _$eventChannelMessageSerializer =
    new _$EventChannelMessageSerializer();
Serializer<EventChannelMessageConnection>
    _$eventChannelMessageConnectionSerializer =
    new _$EventChannelMessageConnectionSerializer();
Serializer<EventChannelMessageEdge> _$eventChannelMessageEdgeSerializer =
    new _$EventChannelMessageEdgeSerializer();
Serializer<PersonalChannel> _$personalChannelSerializer =
    new _$PersonalChannelSerializer();
Serializer<PersonalChannelConnection> _$personalChannelConnectionSerializer =
    new _$PersonalChannelConnectionSerializer();
Serializer<PersonalChannelEdge> _$personalChannelEdgeSerializer =
    new _$PersonalChannelEdgeSerializer();
Serializer<PersonalChannelMessage> _$personalChannelMessageSerializer =
    new _$PersonalChannelMessageSerializer();
Serializer<PersonalChannelMessageConnection>
    _$personalChannelMessageConnectionSerializer =
    new _$PersonalChannelMessageConnectionSerializer();
Serializer<PersonalChannelMessageEdge> _$personalChannelMessageEdgeSerializer =
    new _$PersonalChannelMessageEdgeSerializer();
Serializer<PersonalChannelMemberConnection>
    _$personalChannelMemberConnectionSerializer =
    new _$PersonalChannelMemberConnectionSerializer();
Serializer<PersonalChannelMemberEdge> _$personalChannelMemberEdgeSerializer =
    new _$PersonalChannelMemberEdgeSerializer();

class _$PageInfoSerializer implements StructuredSerializer<PageInfo> {
  @override
  final Iterable<Type> types = const [PageInfo, _$PageInfo];
  @override
  final String wireName = 'PageInfo';

  @override
  Iterable serialize(Serializers serializers, PageInfo object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'startCursor',
      serializers.serialize(object.startCursor,
          specifiedType: const FullType(String)),
      'endCursor',
      serializers.serialize(object.endCursor,
          specifiedType: const FullType(String)),
      'hasNextPage',
      serializers.serialize(object.hasNextPage,
          specifiedType: const FullType(bool)),
      'hasPreviousPage',
      serializers.serialize(object.hasPreviousPage,
          specifiedType: const FullType(bool)),
    ];

    return result;
  }

  @override
  PageInfo deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PageInfoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'startCursor':
          result.startCursor = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'endCursor':
          result.endCursor = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'hasNextPage':
          result.hasNextPage = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'hasPreviousPage':
          result.hasPreviousPage = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$ChannelMessageStateSerializer
    implements PrimitiveSerializer<ChannelMessageState> {
  @override
  final Iterable<Type> types = const <Type>[ChannelMessageState];
  @override
  final String wireName = 'ChannelMessageState';

  @override
  Object serialize(Serializers serializers, ChannelMessageState object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  ChannelMessageState deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ChannelMessageState.valueOf(serialized as String);
}

class _$ChannelMessageTypeSerializer
    implements PrimitiveSerializer<ChannelMessageType> {
  @override
  final Iterable<Type> types = const <Type>[ChannelMessageType];
  @override
  final String wireName = 'ChannelMessageType';

  @override
  Object serialize(Serializers serializers, ChannelMessageType object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  ChannelMessageType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ChannelMessageType.valueOf(serialized as String);
}

class _$DateTimePeriodSerializer
    implements StructuredSerializer<DateTimePeriod> {
  @override
  final Iterable<Type> types = const [DateTimePeriod, _$DateTimePeriod];
  @override
  final String wireName = 'DateTimePeriod';

  @override
  Iterable serialize(Serializers serializers, DateTimePeriod object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'start',
      serializers.serialize(object.start,
          specifiedType: const FullType(DateTime)),
      'end',
      serializers.serialize(object.end,
          specifiedType: const FullType(DateTime)),
    ];

    return result;
  }

  @override
  DateTimePeriod deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DateTimePeriodBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'start':
          result.start = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'end':
          result.end = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
      }
    }

    return result.build();
  }
}

class _$UserSerializer implements StructuredSerializer<User> {
  @override
  final Iterable<Type> types = const [User, _$User];
  @override
  final String wireName = 'User';

  @override
  Iterable serialize(Serializers serializers, User object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'name',
      serializers.serialize(object.name,
          specifiedType: const FullType(UserName)),
      'gender',
      serializers.serialize(object.gender,
          specifiedType: const FullType(UserGender)),
      'school',
      serializers.serialize(object.school,
          specifiedType: const FullType(School)),
      'headshotURL',
      serializers.serialize(object.headshotURL,
          specifiedType: const FullType(String)),
      'birth',
      serializers.serialize(object.birth,
          specifiedType: const FullType(DateTime)),
      'relationship',
      serializers.serialize(object.relationship,
          specifiedType: const FullType(UserRelationship)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  User deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'name':
          result.name.replace(serializers.deserialize(value,
              specifiedType: const FullType(UserName)) as UserName);
          break;
        case 'gender':
          result.gender = serializers.deserialize(value,
              specifiedType: const FullType(UserGender)) as UserGender;
          break;
        case 'school':
          result.school.replace(serializers.deserialize(value,
              specifiedType: const FullType(School)) as School);
          break;
        case 'headshotURL':
          result.headshotURL = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'birth':
          result.birth = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'relationship':
          result.relationship = serializers.deserialize(value,
                  specifiedType: const FullType(UserRelationship))
              as UserRelationship;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$UserNameSerializer implements StructuredSerializer<UserName> {
  @override
  final Iterable<Type> types = const [UserName, _$UserName];
  @override
  final String wireName = 'UserName';

  @override
  Iterable serialize(Serializers serializers, UserName object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'displayName',
      serializers.serialize(object.displayName,
          specifiedType: const FullType(String)),
      'firstName',
      serializers.serialize(object.firstName,
          specifiedType: const FullType(String)),
      'lastName',
      serializers.serialize(object.lastName,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  UserName deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserNameBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'displayName':
          result.displayName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'firstName':
          result.firstName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'lastName':
          result.lastName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$UserGenderSerializer implements PrimitiveSerializer<UserGender> {
  @override
  final Iterable<Type> types = const <Type>[UserGender];
  @override
  final String wireName = 'UserGender';

  @override
  Object serialize(Serializers serializers, UserGender object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  UserGender deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      UserGender.valueOf(serialized as String);
}

class _$UserRelationshipSerializer
    implements PrimitiveSerializer<UserRelationship> {
  @override
  final Iterable<Type> types = const <Type>[UserRelationship];
  @override
  final String wireName = 'UserRelationship';

  @override
  Object serialize(Serializers serializers, UserRelationship object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  UserRelationship deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      UserRelationship.valueOf(serialized as String);
}

class _$UserEventStateSerializer
    implements PrimitiveSerializer<UserEventState> {
  @override
  final Iterable<Type> types = const <Type>[UserEventState];
  @override
  final String wireName = 'UserEventState';

  @override
  Object serialize(Serializers serializers, UserEventState object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  UserEventState deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      UserEventState.valueOf(serialized as String);
}

class _$UserEventConnectionSerializer
    implements StructuredSerializer<UserEventConnection> {
  @override
  final Iterable<Type> types = const [
    UserEventConnection,
    _$UserEventConnection
  ];
  @override
  final String wireName = 'UserEventConnection';

  @override
  Iterable serialize(Serializers serializers, UserEventConnection object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'edges',
      serializers.serialize(object.edges,
          specifiedType:
              const FullType(BuiltList, const [const FullType(UserEventEdge)])),
      'nodes',
      serializers.serialize(object.nodes,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Event)])),
      'pageInfo',
      serializers.serialize(object.pageInfo,
          specifiedType: const FullType(PageInfo)),
      'count',
      serializers.serialize(object.count, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  UserEventConnection deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserEventConnectionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'edges':
          result.edges.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(UserEventEdge)]))
              as BuiltList);
          break;
        case 'nodes':
          result.nodes.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(Event)]))
              as BuiltList);
          break;
        case 'pageInfo':
          result.pageInfo.replace(serializers.deserialize(value,
              specifiedType: const FullType(PageInfo)) as PageInfo);
          break;
        case 'count':
          result.count = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$UserEventEdgeSerializer implements StructuredSerializer<UserEventEdge> {
  @override
  final Iterable<Type> types = const [UserEventEdge, _$UserEventEdge];
  @override
  final String wireName = 'UserEventEdge';

  @override
  Iterable serialize(Serializers serializers, UserEventEdge object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'node',
      serializers.serialize(object.node, specifiedType: const FullType(Event)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(DateTime)),
      'state',
      serializers.serialize(object.state,
          specifiedType: const FullType(UserEventState)),
      'cursor',
      serializers.serialize(object.cursor,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  UserEventEdge deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserEventEdgeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'node':
          result.node.replace(serializers.deserialize(value,
              specifiedType: const FullType(Event)) as Event);
          break;
        case 'createdAt':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'state':
          result.state = serializers.deserialize(value,
              specifiedType: const FullType(UserEventState)) as UserEventState;
          break;
        case 'cursor':
          result.cursor = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$UserFriendConnectionSerializer
    implements StructuredSerializer<UserFriendConnection> {
  @override
  final Iterable<Type> types = const [
    UserFriendConnection,
    _$UserFriendConnection
  ];
  @override
  final String wireName = 'UserFriendConnection';

  @override
  Iterable serialize(Serializers serializers, UserFriendConnection object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'edges',
      serializers.serialize(object.edges,
          specifiedType: const FullType(
              BuiltList, const [const FullType(UserFriendEdge)])),
      'nodes',
      serializers.serialize(object.nodes,
          specifiedType:
              const FullType(BuiltList, const [const FullType(User)])),
      'pageInfo',
      serializers.serialize(object.pageInfo,
          specifiedType: const FullType(PageInfo)),
      'count',
      serializers.serialize(object.count, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  UserFriendConnection deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserFriendConnectionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'edges':
          result.edges.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(UserFriendEdge)]))
              as BuiltList);
          break;
        case 'nodes':
          result.nodes.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(User)]))
              as BuiltList);
          break;
        case 'pageInfo':
          result.pageInfo.replace(serializers.deserialize(value,
              specifiedType: const FullType(PageInfo)) as PageInfo);
          break;
        case 'count':
          result.count = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$UserFieldConnectionSerializer
    implements StructuredSerializer<UserFieldConnection> {
  @override
  final Iterable<Type> types = const [
    UserFieldConnection,
    _$UserFieldConnection
  ];
  @override
  final String wireName = 'UserFieldConnection';

  @override
  Iterable serialize(Serializers serializers, UserFieldConnection object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'edges',
      serializers.serialize(object.edges,
          specifiedType:
              const FullType(BuiltList, const [const FullType(UserFieldEdge)])),
      'nodes',
      serializers.serialize(object.nodes,
          specifiedType:
              const FullType(BuiltList, const [const FullType(SchoolField)])),
      'pageInfo',
      serializers.serialize(object.pageInfo,
          specifiedType: const FullType(PageInfo)),
      'count',
      serializers.serialize(object.count, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  UserFieldConnection deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserFieldConnectionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'edges':
          result.edges.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(UserFieldEdge)]))
              as BuiltList);
          break;
        case 'nodes':
          result.nodes.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(SchoolField)]))
              as BuiltList);
          break;
        case 'pageInfo':
          result.pageInfo.replace(serializers.deserialize(value,
              specifiedType: const FullType(PageInfo)) as PageInfo);
          break;
        case 'count':
          result.count = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$UserFriendEdgeSerializer
    implements StructuredSerializer<UserFriendEdge> {
  @override
  final Iterable<Type> types = const [UserFriendEdge, _$UserFriendEdge];
  @override
  final String wireName = 'UserFriendEdge';

  @override
  Iterable serialize(Serializers serializers, UserFriendEdge object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'node',
      serializers.serialize(object.node, specifiedType: const FullType(User)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(DateTime)),
      'cursor',
      serializers.serialize(object.cursor,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  UserFriendEdge deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserFriendEdgeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'node':
          result.node.replace(serializers.deserialize(value,
              specifiedType: const FullType(User)) as User);
          break;
        case 'createdAt':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'cursor':
          result.cursor = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$UserFieldEdgeSerializer implements StructuredSerializer<UserFieldEdge> {
  @override
  final Iterable<Type> types = const [UserFieldEdge, _$UserFieldEdge];
  @override
  final String wireName = 'UserFieldEdge';

  @override
  Iterable serialize(Serializers serializers, UserFieldEdge object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'node',
      serializers.serialize(object.node,
          specifiedType: const FullType(SchoolField)),
      'state',
      serializers.serialize(object.state,
          specifiedType: const FullType(UserFieldState)),
      'cursor',
      serializers.serialize(object.cursor,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  UserFieldEdge deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserFieldEdgeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'node':
          result.node.replace(serializers.deserialize(value,
              specifiedType: const FullType(SchoolField)) as SchoolField);
          break;
        case 'state':
          result.state = serializers.deserialize(value,
              specifiedType: const FullType(UserFieldState)) as UserFieldState;
          break;
        case 'cursor':
          result.cursor = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$UserFieldStateSerializer
    implements PrimitiveSerializer<UserFieldState> {
  @override
  final Iterable<Type> types = const <Type>[UserFieldState];
  @override
  final String wireName = 'UserFieldState';

  @override
  Object serialize(Serializers serializers, UserFieldState object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  UserFieldState deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      UserFieldState.valueOf(serialized as String);
}

class _$SchoolSerializer implements StructuredSerializer<School> {
  @override
  final Iterable<Type> types = const [School, _$School];
  @override
  final String wireName = 'School';

  @override
  Iterable serialize(Serializers serializers, School object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  School deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SchoolBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$SchoolFieldSerializer implements StructuredSerializer<SchoolField> {
  @override
  final Iterable<Type> types = const [SchoolField, _$SchoolField];
  @override
  final String wireName = 'SchoolField';

  @override
  Iterable serialize(Serializers serializers, SchoolField object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'school',
      serializers.serialize(object.school,
          specifiedType: const FullType(School)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'fields',
      serializers.serialize(object.fields,
          specifiedType:
              const FullType(BuiltList, const [const FullType(StudyField)])),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  SchoolField deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SchoolFieldBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'school':
          result.school.replace(serializers.deserialize(value,
              specifiedType: const FullType(School)) as School);
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'fields':
          result.fields.replace(serializers.deserialize(value,
              specifiedType: const FullType(
                  BuiltList, const [const FullType(StudyField)])) as BuiltList);
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$SchoolFieldConnectionSerializer
    implements StructuredSerializer<SchoolFieldConnection> {
  @override
  final Iterable<Type> types = const [
    SchoolFieldConnection,
    _$SchoolFieldConnection
  ];
  @override
  final String wireName = 'SchoolFieldConnection';

  @override
  Iterable serialize(Serializers serializers, SchoolFieldConnection object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'edges',
      serializers.serialize(object.edges,
          specifiedType: const FullType(
              BuiltList, const [const FullType(SchoolFieldEdge)])),
      'nodes',
      serializers.serialize(object.nodes,
          specifiedType:
              const FullType(BuiltList, const [const FullType(SchoolField)])),
      'pageInfo',
      serializers.serialize(object.pageInfo,
          specifiedType: const FullType(PageInfo)),
      'count',
      serializers.serialize(object.count, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  SchoolFieldConnection deserialize(
      Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SchoolFieldConnectionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'edges':
          result.edges.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(SchoolFieldEdge)]))
              as BuiltList);
          break;
        case 'nodes':
          result.nodes.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(SchoolField)]))
              as BuiltList);
          break;
        case 'pageInfo':
          result.pageInfo.replace(serializers.deserialize(value,
              specifiedType: const FullType(PageInfo)) as PageInfo);
          break;
        case 'count':
          result.count = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$SchoolFieldEdgeSerializer
    implements StructuredSerializer<SchoolFieldEdge> {
  @override
  final Iterable<Type> types = const [SchoolFieldEdge, _$SchoolFieldEdge];
  @override
  final String wireName = 'SchoolFieldEdge';

  @override
  Iterable serialize(Serializers serializers, SchoolFieldEdge object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'node',
      serializers.serialize(object.node,
          specifiedType: const FullType(SchoolField)),
      'cursor',
      serializers.serialize(object.cursor,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  SchoolFieldEdge deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SchoolFieldEdgeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'node':
          result.node.replace(serializers.deserialize(value,
              specifiedType: const FullType(SchoolField)) as SchoolField);
          break;
        case 'cursor':
          result.cursor = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$StudyFieldSerializer implements PrimitiveSerializer<StudyField> {
  @override
  final Iterable<Type> types = const <Type>[StudyField];
  @override
  final String wireName = 'StudyField';

  @override
  Object serialize(Serializers serializers, StudyField object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  StudyField deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      StudyField.valueOf(serialized as String);
}

class _$EventSerializer implements StructuredSerializer<Event> {
  @override
  final Iterable<Type> types = const [Event, _$Event];
  @override
  final String wireName = 'Event';

  @override
  Iterable serialize(Serializers serializers, Event object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'description',
      serializers.serialize(object.description,
          specifiedType: const FullType(String)),
      'hostDate',
      serializers.serialize(object.hostDate,
          specifiedType: const FullType(DateTimePeriod)),
      'channel',
      serializers.serialize(object.channel,
          specifiedType: const FullType(EventChannel)),
      'state',
      serializers.serialize(object.state,
          specifiedType: const FullType(EventState)),
      'viewerState',
      serializers.serialize(object.viewerState,
          specifiedType: const FullType(UserEventState)),
      'viewerCanJoin',
      serializers.serialize(object.viewerCanJoin,
          specifiedType: const FullType(bool)),
      'viewerCanFollow',
      serializers.serialize(object.viewerCanFollow,
          specifiedType: const FullType(bool)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  Event deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new EventBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'hostDate':
          result.hostDate.replace(serializers.deserialize(value,
              specifiedType: const FullType(DateTimePeriod)) as DateTimePeriod);
          break;
        case 'channel':
          result.channel.replace(serializers.deserialize(value,
              specifiedType: const FullType(EventChannel)) as EventChannel);
          break;
        case 'state':
          result.state = serializers.deserialize(value,
              specifiedType: const FullType(EventState)) as EventState;
          break;
        case 'viewerState':
          result.viewerState = serializers.deserialize(value,
              specifiedType: const FullType(UserEventState)) as UserEventState;
          break;
        case 'viewerCanJoin':
          result.viewerCanJoin = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'viewerCanFollow':
          result.viewerCanFollow = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$EventStateSerializer implements PrimitiveSerializer<EventState> {
  @override
  final Iterable<Type> types = const <Type>[EventState];
  @override
  final String wireName = 'EventState';

  @override
  Object serialize(Serializers serializers, EventState object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  EventState deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      EventState.valueOf(serialized as String);
}

class _$EventHostConnectionSerializer
    implements StructuredSerializer<EventHostConnection> {
  @override
  final Iterable<Type> types = const [
    EventHostConnection,
    _$EventHostConnection
  ];
  @override
  final String wireName = 'EventHostConnection';

  @override
  Iterable serialize(Serializers serializers, EventHostConnection object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'edges',
      serializers.serialize(object.edges,
          specifiedType:
              const FullType(BuiltList, const [const FullType(EventHostEdge)])),
      'nodes',
      serializers.serialize(object.nodes,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Actor)])),
      'pageInfo',
      serializers.serialize(object.pageInfo,
          specifiedType: const FullType(PageInfo)),
      'count',
      serializers.serialize(object.count, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  EventHostConnection deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new EventHostConnectionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'edges':
          result.edges.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(EventHostEdge)]))
              as BuiltList);
          break;
        case 'nodes':
          result.nodes.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(Actor)]))
              as BuiltList);
          break;
        case 'pageInfo':
          result.pageInfo.replace(serializers.deserialize(value,
              specifiedType: const FullType(PageInfo)) as PageInfo);
          break;
        case 'count':
          result.count = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$EventHostEdgeSerializer implements StructuredSerializer<EventHostEdge> {
  @override
  final Iterable<Type> types = const [EventHostEdge, _$EventHostEdge];
  @override
  final String wireName = 'EventHostEdge';

  @override
  Iterable serialize(Serializers serializers, EventHostEdge object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'node',
      serializers.serialize(object.node, specifiedType: const FullType(Actor)),
      'cursor',
      serializers.serialize(object.cursor,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  EventHostEdge deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new EventHostEdgeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'node':
          result.node = serializers.deserialize(value,
              specifiedType: const FullType(Actor)) as Actor;
          break;
        case 'cursor':
          result.cursor = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$EventFriendConnectionSerializer
    implements StructuredSerializer<EventFriendConnection> {
  @override
  final Iterable<Type> types = const [
    EventFriendConnection,
    _$EventFriendConnection
  ];
  @override
  final String wireName = 'EventFriendConnection';

  @override
  Iterable serialize(Serializers serializers, EventFriendConnection object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'edges',
      serializers.serialize(object.edges,
          specifiedType: const FullType(
              BuiltList, const [const FullType(EventFriendEdge)])),
      'nodes',
      serializers.serialize(object.nodes,
          specifiedType:
              const FullType(BuiltList, const [const FullType(User)])),
      'pageInfo',
      serializers.serialize(object.pageInfo,
          specifiedType: const FullType(PageInfo)),
      'count',
      serializers.serialize(object.count, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  EventFriendConnection deserialize(
      Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new EventFriendConnectionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'edges':
          result.edges.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(EventFriendEdge)]))
              as BuiltList);
          break;
        case 'nodes':
          result.nodes.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(User)]))
              as BuiltList);
          break;
        case 'pageInfo':
          result.pageInfo.replace(serializers.deserialize(value,
              specifiedType: const FullType(PageInfo)) as PageInfo);
          break;
        case 'count':
          result.count = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$EventFriendEdgeSerializer
    implements StructuredSerializer<EventFriendEdge> {
  @override
  final Iterable<Type> types = const [EventFriendEdge, _$EventFriendEdge];
  @override
  final String wireName = 'EventFriendEdge';

  @override
  Iterable serialize(Serializers serializers, EventFriendEdge object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'node',
      serializers.serialize(object.node, specifiedType: const FullType(User)),
      'state',
      serializers.serialize(object.state,
          specifiedType: const FullType(UserEventState)),
      'cursor',
      serializers.serialize(object.cursor,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  EventFriendEdge deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new EventFriendEdgeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'node':
          result.node.replace(serializers.deserialize(value,
              specifiedType: const FullType(User)) as User);
          break;
        case 'state':
          result.state = serializers.deserialize(value,
              specifiedType: const FullType(UserEventState)) as UserEventState;
          break;
        case 'cursor':
          result.cursor = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$EventChannelSerializer implements StructuredSerializer<EventChannel> {
  @override
  final Iterable<Type> types = const [EventChannel, _$EventChannel];
  @override
  final String wireName = 'EventChannel';

  @override
  Iterable serialize(Serializers serializers, EventChannel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'coverUrl',
      serializers.serialize(object.coverUrl,
          specifiedType: const FullType(String)),
      'viewerCanAdminister',
      serializers.serialize(object.viewerCanAdminister,
          specifiedType: const FullType(bool)),
      'viewerCanChat',
      serializers.serialize(object.viewerCanChat,
          specifiedType: const FullType(bool)),
      'viewerIsMember',
      serializers.serialize(object.viewerIsMember,
          specifiedType: const FullType(bool)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'viewerLastReadAt',
      serializers.serialize(object.viewerLastReadAt,
          specifiedType: const FullType(DateTime)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(DateTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(DateTime)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  EventChannel deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new EventChannelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'coverUrl':
          result.coverUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'viewerCanAdminister':
          result.viewerCanAdminister = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'viewerCanChat':
          result.viewerCanChat = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'viewerIsMember':
          result.viewerIsMember = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'viewerLastReadAt':
          result.viewerLastReadAt = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'createdAt':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'updatedAt':
          result.updatedAt = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$EventChannelMemberConnectionSerializer
    implements StructuredSerializer<EventChannelMemberConnection> {
  @override
  final Iterable<Type> types = const [
    EventChannelMemberConnection,
    _$EventChannelMemberConnection
  ];
  @override
  final String wireName = 'EventChannelMemberConnection';

  @override
  Iterable serialize(
      Serializers serializers, EventChannelMemberConnection object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'edges',
      serializers.serialize(object.edges,
          specifiedType: const FullType(
              BuiltList, const [const FullType(EventChannelMemberEdge)])),
      'nodes',
      serializers.serialize(object.nodes,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Actor)])),
      'pageInfo',
      serializers.serialize(object.pageInfo,
          specifiedType: const FullType(PageInfo)),
      'count',
      serializers.serialize(object.count, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  EventChannelMemberConnection deserialize(
      Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new EventChannelMemberConnectionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'edges':
          result.edges.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(EventChannelMemberEdge)
              ])) as BuiltList);
          break;
        case 'nodes':
          result.nodes.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(Actor)]))
              as BuiltList);
          break;
        case 'pageInfo':
          result.pageInfo.replace(serializers.deserialize(value,
              specifiedType: const FullType(PageInfo)) as PageInfo);
          break;
        case 'count':
          result.count = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$EventChannelMemberEdgeSerializer
    implements StructuredSerializer<EventChannelMemberEdge> {
  @override
  final Iterable<Type> types = const [
    EventChannelMemberEdge,
    _$EventChannelMemberEdge
  ];
  @override
  final String wireName = 'EventChannelMemberEdge';

  @override
  Iterable serialize(Serializers serializers, EventChannelMemberEdge object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'node',
      serializers.serialize(object.node, specifiedType: const FullType(Actor)),
      'state',
      serializers.serialize(object.state,
          specifiedType: const FullType(UserEventState)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(DateTime)),
      'cursor',
      serializers.serialize(object.cursor,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  EventChannelMemberEdge deserialize(
      Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new EventChannelMemberEdgeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'node':
          result.node = serializers.deserialize(value,
              specifiedType: const FullType(Actor)) as Actor;
          break;
        case 'state':
          result.state = serializers.deserialize(value,
              specifiedType: const FullType(UserEventState)) as UserEventState;
          break;
        case 'createdAt':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'cursor':
          result.cursor = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$EventChannelMessageSerializer
    implements StructuredSerializer<EventChannelMessage> {
  @override
  final Iterable<Type> types = const [
    EventChannelMessage,
    _$EventChannelMessage
  ];
  @override
  final String wireName = 'EventChannelMessage';

  @override
  Iterable serialize(Serializers serializers, EventChannelMessage object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'channel',
      serializers.serialize(object.channel,
          specifiedType: const FullType(Channel)),
      'author',
      serializers.serialize(object.author,
          specifiedType: const FullType(Actor)),
      'body',
      serializers.serialize(object.body, specifiedType: const FullType(String)),
      'state',
      serializers.serialize(object.state,
          specifiedType: const FullType(ChannelMessageState)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(DateTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(DateTime)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  EventChannelMessage deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new EventChannelMessageBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'channel':
          result.channel = serializers.deserialize(value,
              specifiedType: const FullType(Channel)) as Channel;
          break;
        case 'author':
          result.author = serializers.deserialize(value,
              specifiedType: const FullType(Actor)) as Actor;
          break;
        case 'body':
          result.body = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'state':
          result.state = serializers.deserialize(value,
                  specifiedType: const FullType(ChannelMessageState))
              as ChannelMessageState;
          break;
        case 'createdAt':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'updatedAt':
          result.updatedAt = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$EventChannelMessageConnectionSerializer
    implements StructuredSerializer<EventChannelMessageConnection> {
  @override
  final Iterable<Type> types = const [
    EventChannelMessageConnection,
    _$EventChannelMessageConnection
  ];
  @override
  final String wireName = 'EventChannelMessageConnection';

  @override
  Iterable serialize(
      Serializers serializers, EventChannelMessageConnection object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'edges',
      serializers.serialize(object.edges,
          specifiedType: const FullType(
              BuiltList, const [const FullType(EventChannelMessageEdge)])),
      'nodes',
      serializers.serialize(object.nodes,
          specifiedType: const FullType(
              BuiltList, const [const FullType(EventChannelMessage)])),
      'pageInfo',
      serializers.serialize(object.pageInfo,
          specifiedType: const FullType(PageInfo)),
      'count',
      serializers.serialize(object.count, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  EventChannelMessageConnection deserialize(
      Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new EventChannelMessageConnectionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'edges':
          result.edges.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(EventChannelMessageEdge)
              ])) as BuiltList);
          break;
        case 'nodes':
          result.nodes.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(EventChannelMessage)]))
              as BuiltList);
          break;
        case 'pageInfo':
          result.pageInfo.replace(serializers.deserialize(value,
              specifiedType: const FullType(PageInfo)) as PageInfo);
          break;
        case 'count':
          result.count = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$EventChannelMessageEdgeSerializer
    implements StructuredSerializer<EventChannelMessageEdge> {
  @override
  final Iterable<Type> types = const [
    EventChannelMessageEdge,
    _$EventChannelMessageEdge
  ];
  @override
  final String wireName = 'EventChannelMessageEdge';

  @override
  Iterable serialize(Serializers serializers, EventChannelMessageEdge object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'node',
      serializers.serialize(object.node,
          specifiedType: const FullType(EventChannelMessage)),
      'cursor',
      serializers.serialize(object.cursor,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  EventChannelMessageEdge deserialize(
      Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new EventChannelMessageEdgeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'node':
          result.node.replace(serializers.deserialize(value,
                  specifiedType: const FullType(EventChannelMessage))
              as EventChannelMessage);
          break;
        case 'cursor':
          result.cursor = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$PersonalChannelSerializer
    implements StructuredSerializer<PersonalChannel> {
  @override
  final Iterable<Type> types = const [PersonalChannel, _$PersonalChannel];
  @override
  final String wireName = 'PersonalChannel';

  @override
  Iterable serialize(Serializers serializers, PersonalChannel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'viewerLastReadAt',
      serializers.serialize(object.viewerLastReadAt,
          specifiedType: const FullType(DateTime)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(DateTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(DateTime)),
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  PersonalChannel deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PersonalChannelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'viewerLastReadAt':
          result.viewerLastReadAt = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'createdAt':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'updatedAt':
          result.updatedAt = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$PersonalChannelConnectionSerializer
    implements StructuredSerializer<PersonalChannelConnection> {
  @override
  final Iterable<Type> types = const [
    PersonalChannelConnection,
    _$PersonalChannelConnection
  ];
  @override
  final String wireName = 'PersonalChannelConnection';

  @override
  Iterable serialize(Serializers serializers, PersonalChannelConnection object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'edges',
      serializers.serialize(object.edges,
          specifiedType: const FullType(
              BuiltList, const [const FullType(PersonalChannelEdge)])),
      'nodes',
      serializers.serialize(object.nodes,
          specifiedType: const FullType(
              BuiltList, const [const FullType(PersonalChannel)])),
      'pageInfo',
      serializers.serialize(object.pageInfo,
          specifiedType: const FullType(PageInfo)),
      'count',
      serializers.serialize(object.count, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  PersonalChannelConnection deserialize(
      Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PersonalChannelConnectionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'edges':
          result.edges.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(PersonalChannelEdge)]))
              as BuiltList);
          break;
        case 'nodes':
          result.nodes.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(PersonalChannel)]))
              as BuiltList);
          break;
        case 'pageInfo':
          result.pageInfo.replace(serializers.deserialize(value,
              specifiedType: const FullType(PageInfo)) as PageInfo);
          break;
        case 'count':
          result.count = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$PersonalChannelEdgeSerializer
    implements StructuredSerializer<PersonalChannelEdge> {
  @override
  final Iterable<Type> types = const [
    PersonalChannelEdge,
    _$PersonalChannelEdge
  ];
  @override
  final String wireName = 'PersonalChannelEdge';

  @override
  Iterable serialize(Serializers serializers, PersonalChannelEdge object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'node',
      serializers.serialize(object.node,
          specifiedType: const FullType(PersonalChannel)),
      'cursor',
      serializers.serialize(object.cursor,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  PersonalChannelEdge deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PersonalChannelEdgeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'node':
          result.node.replace(serializers.deserialize(value,
                  specifiedType: const FullType(PersonalChannel))
              as PersonalChannel);
          break;
        case 'cursor':
          result.cursor = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$PersonalChannelMessageSerializer
    implements StructuredSerializer<PersonalChannelMessage> {
  @override
  final Iterable<Type> types = const [
    PersonalChannelMessage,
    _$PersonalChannelMessage
  ];
  @override
  final String wireName = 'PersonalChannelMessage';

  @override
  Iterable serialize(Serializers serializers, PersonalChannelMessage object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'channel',
      serializers.serialize(object.channel,
          specifiedType: const FullType(Channel)),
      'author',
      serializers.serialize(object.author,
          specifiedType: const FullType(Actor)),
      'body',
      serializers.serialize(object.body, specifiedType: const FullType(String)),
      'state',
      serializers.serialize(object.state,
          specifiedType: const FullType(ChannelMessageState)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(DateTime)),
      'updatedAt',
      serializers.serialize(object.updatedAt,
          specifiedType: const FullType(DateTime)),
    ];

    return result;
  }

  @override
  PersonalChannelMessage deserialize(
      Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PersonalChannelMessageBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'channel':
          result.channel = serializers.deserialize(value,
              specifiedType: const FullType(Channel)) as Channel;
          break;
        case 'author':
          result.author = serializers.deserialize(value,
              specifiedType: const FullType(Actor)) as Actor;
          break;
        case 'body':
          result.body = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'state':
          result.state = serializers.deserialize(value,
                  specifiedType: const FullType(ChannelMessageState))
              as ChannelMessageState;
          break;
        case 'createdAt':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'updatedAt':
          result.updatedAt = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
      }
    }

    return result.build();
  }
}

class _$PersonalChannelMessageConnectionSerializer
    implements StructuredSerializer<PersonalChannelMessageConnection> {
  @override
  final Iterable<Type> types = const [
    PersonalChannelMessageConnection,
    _$PersonalChannelMessageConnection
  ];
  @override
  final String wireName = 'PersonalChannelMessageConnection';

  @override
  Iterable serialize(
      Serializers serializers, PersonalChannelMessageConnection object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'edges',
      serializers.serialize(object.edges,
          specifiedType: const FullType(
              BuiltList, const [const FullType(PersonalChannelMessageEdge)])),
      'nodes',
      serializers.serialize(object.nodes,
          specifiedType: const FullType(
              BuiltList, const [const FullType(PersonalChannelMessage)])),
      'node',
      serializers.serialize(object.node,
          specifiedType: const FullType(ChannelMessage)),
      'cursor',
      serializers.serialize(object.cursor,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  PersonalChannelMessageConnection deserialize(
      Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PersonalChannelMessageConnectionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'edges':
          result.edges.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(PersonalChannelMessageEdge)
              ])) as BuiltList);
          break;
        case 'nodes':
          result.nodes.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(PersonalChannelMessage)
              ])) as BuiltList);
          break;
        case 'node':
          result.node = serializers.deserialize(value,
              specifiedType: const FullType(ChannelMessage)) as ChannelMessage;
          break;
        case 'cursor':
          result.cursor = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$PersonalChannelMessageEdgeSerializer
    implements StructuredSerializer<PersonalChannelMessageEdge> {
  @override
  final Iterable<Type> types = const [
    PersonalChannelMessageEdge,
    _$PersonalChannelMessageEdge
  ];
  @override
  final String wireName = 'PersonalChannelMessageEdge';

  @override
  Iterable serialize(Serializers serializers, PersonalChannelMessageEdge object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'node',
      serializers.serialize(object.node,
          specifiedType: const FullType(PersonalChannelMessage)),
      'cursor',
      serializers.serialize(object.cursor,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  PersonalChannelMessageEdge deserialize(
      Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PersonalChannelMessageEdgeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'node':
          result.node.replace(serializers.deserialize(value,
                  specifiedType: const FullType(PersonalChannelMessage))
              as PersonalChannelMessage);
          break;
        case 'cursor':
          result.cursor = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$PersonalChannelMemberConnectionSerializer
    implements StructuredSerializer<PersonalChannelMemberConnection> {
  @override
  final Iterable<Type> types = const [
    PersonalChannelMemberConnection,
    _$PersonalChannelMemberConnection
  ];
  @override
  final String wireName = 'PersonalChannelMemberConnection';

  @override
  Iterable serialize(
      Serializers serializers, PersonalChannelMemberConnection object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'edges',
      serializers.serialize(object.edges,
          specifiedType: const FullType(
              BuiltList, const [const FullType(PersonalChannelMemberEdge)])),
      'nodes',
      serializers.serialize(object.nodes,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Actor)])),
      'pageInfo',
      serializers.serialize(object.pageInfo,
          specifiedType: const FullType(PageInfo)),
      'count',
      serializers.serialize(object.count, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  PersonalChannelMemberConnection deserialize(
      Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PersonalChannelMemberConnectionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'edges':
          result.edges.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(PersonalChannelMemberEdge)
              ])) as BuiltList);
          break;
        case 'nodes':
          result.nodes.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(Actor)]))
              as BuiltList);
          break;
        case 'pageInfo':
          result.pageInfo.replace(serializers.deserialize(value,
              specifiedType: const FullType(PageInfo)) as PageInfo);
          break;
        case 'count':
          result.count = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$PersonalChannelMemberEdgeSerializer
    implements StructuredSerializer<PersonalChannelMemberEdge> {
  @override
  final Iterable<Type> types = const [
    PersonalChannelMemberEdge,
    _$PersonalChannelMemberEdge
  ];
  @override
  final String wireName = 'PersonalChannelMemberEdge';

  @override
  Iterable serialize(Serializers serializers, PersonalChannelMemberEdge object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'node',
      serializers.serialize(object.node, specifiedType: const FullType(Actor)),
      'createdAt',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(DateTime)),
      'cursor',
      serializers.serialize(object.cursor,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  PersonalChannelMemberEdge deserialize(
      Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PersonalChannelMemberEdgeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'node':
          result.node = serializers.deserialize(value,
              specifiedType: const FullType(Actor)) as Actor;
          break;
        case 'createdAt':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'cursor':
          result.cursor = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$PageInfo extends PageInfo {
  @override
  final String startCursor;
  @override
  final String endCursor;
  @override
  final bool hasNextPage;
  @override
  final bool hasPreviousPage;

  factory _$PageInfo([void updates(PageInfoBuilder b)]) =>
      (new PageInfoBuilder()..update(updates)).build();

  _$PageInfo._(
      {this.startCursor,
      this.endCursor,
      this.hasNextPage,
      this.hasPreviousPage})
      : super._() {
    if (startCursor == null) {
      throw new BuiltValueNullFieldError('PageInfo', 'startCursor');
    }
    if (endCursor == null) {
      throw new BuiltValueNullFieldError('PageInfo', 'endCursor');
    }
    if (hasNextPage == null) {
      throw new BuiltValueNullFieldError('PageInfo', 'hasNextPage');
    }
    if (hasPreviousPage == null) {
      throw new BuiltValueNullFieldError('PageInfo', 'hasPreviousPage');
    }
  }

  @override
  PageInfo rebuild(void updates(PageInfoBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  PageInfoBuilder toBuilder() => new PageInfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PageInfo &&
        startCursor == other.startCursor &&
        endCursor == other.endCursor &&
        hasNextPage == other.hasNextPage &&
        hasPreviousPage == other.hasPreviousPage;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, startCursor.hashCode), endCursor.hashCode),
            hasNextPage.hashCode),
        hasPreviousPage.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PageInfo')
          ..add('startCursor', startCursor)
          ..add('endCursor', endCursor)
          ..add('hasNextPage', hasNextPage)
          ..add('hasPreviousPage', hasPreviousPage))
        .toString();
  }
}

class PageInfoBuilder implements Builder<PageInfo, PageInfoBuilder> {
  _$PageInfo _$v;

  String _startCursor;
  String get startCursor => _$this._startCursor;
  set startCursor(String startCursor) => _$this._startCursor = startCursor;

  String _endCursor;
  String get endCursor => _$this._endCursor;
  set endCursor(String endCursor) => _$this._endCursor = endCursor;

  bool _hasNextPage;
  bool get hasNextPage => _$this._hasNextPage;
  set hasNextPage(bool hasNextPage) => _$this._hasNextPage = hasNextPage;

  bool _hasPreviousPage;
  bool get hasPreviousPage => _$this._hasPreviousPage;
  set hasPreviousPage(bool hasPreviousPage) =>
      _$this._hasPreviousPage = hasPreviousPage;

  PageInfoBuilder();

  PageInfoBuilder get _$this {
    if (_$v != null) {
      _startCursor = _$v.startCursor;
      _endCursor = _$v.endCursor;
      _hasNextPage = _$v.hasNextPage;
      _hasPreviousPage = _$v.hasPreviousPage;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PageInfo other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PageInfo;
  }

  @override
  void update(void updates(PageInfoBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$PageInfo build() {
    final _$result = _$v ??
        new _$PageInfo._(
            startCursor: startCursor,
            endCursor: endCursor,
            hasNextPage: hasNextPage,
            hasPreviousPage: hasPreviousPage);
    replace(_$result);
    return _$result;
  }
}

class _$DateTimePeriod extends DateTimePeriod {
  @override
  final DateTime start;
  @override
  final DateTime end;

  factory _$DateTimePeriod([void updates(DateTimePeriodBuilder b)]) =>
      (new DateTimePeriodBuilder()..update(updates)).build();

  _$DateTimePeriod._({this.start, this.end}) : super._() {
    if (start == null) {
      throw new BuiltValueNullFieldError('DateTimePeriod', 'start');
    }
    if (end == null) {
      throw new BuiltValueNullFieldError('DateTimePeriod', 'end');
    }
  }

  @override
  DateTimePeriod rebuild(void updates(DateTimePeriodBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  DateTimePeriodBuilder toBuilder() =>
      new DateTimePeriodBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DateTimePeriod && start == other.start && end == other.end;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, start.hashCode), end.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DateTimePeriod')
          ..add('start', start)
          ..add('end', end))
        .toString();
  }
}

class DateTimePeriodBuilder
    implements Builder<DateTimePeriod, DateTimePeriodBuilder> {
  _$DateTimePeriod _$v;

  DateTime _start;
  DateTime get start => _$this._start;
  set start(DateTime start) => _$this._start = start;

  DateTime _end;
  DateTime get end => _$this._end;
  set end(DateTime end) => _$this._end = end;

  DateTimePeriodBuilder();

  DateTimePeriodBuilder get _$this {
    if (_$v != null) {
      _start = _$v.start;
      _end = _$v.end;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DateTimePeriod other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DateTimePeriod;
  }

  @override
  void update(void updates(DateTimePeriodBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$DateTimePeriod build() {
    final _$result = _$v ?? new _$DateTimePeriod._(start: start, end: end);
    replace(_$result);
    return _$result;
  }
}

class _$User extends User {
  @override
  final UserName name;
  @override
  final UserGender gender;
  @override
  final School school;
  @override
  final String headshotURL;
  @override
  final DateTime birth;
  @override
  final UserRelationship relationship;
  @override
  final String id;

  factory _$User([void updates(UserBuilder b)]) =>
      (new UserBuilder()..update(updates)).build();

  _$User._(
      {this.name,
      this.gender,
      this.school,
      this.headshotURL,
      this.birth,
      this.relationship,
      this.id})
      : super._() {
    if (name == null) {
      throw new BuiltValueNullFieldError('User', 'name');
    }
    if (gender == null) {
      throw new BuiltValueNullFieldError('User', 'gender');
    }
    if (school == null) {
      throw new BuiltValueNullFieldError('User', 'school');
    }
    if (headshotURL == null) {
      throw new BuiltValueNullFieldError('User', 'headshotURL');
    }
    if (birth == null) {
      throw new BuiltValueNullFieldError('User', 'birth');
    }
    if (relationship == null) {
      throw new BuiltValueNullFieldError('User', 'relationship');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('User', 'id');
    }
  }

  @override
  User rebuild(void updates(UserBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  UserBuilder toBuilder() => new UserBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is User &&
        name == other.name &&
        gender == other.gender &&
        school == other.school &&
        headshotURL == other.headshotURL &&
        birth == other.birth &&
        relationship == other.relationship &&
        id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, name.hashCode), gender.hashCode),
                        school.hashCode),
                    headshotURL.hashCode),
                birth.hashCode),
            relationship.hashCode),
        id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('User')
          ..add('name', name)
          ..add('gender', gender)
          ..add('school', school)
          ..add('headshotURL', headshotURL)
          ..add('birth', birth)
          ..add('relationship', relationship)
          ..add('id', id))
        .toString();
  }
}

class UserBuilder implements Builder<User, UserBuilder> {
  _$User _$v;

  UserNameBuilder _name;
  UserNameBuilder get name => _$this._name ??= new UserNameBuilder();
  set name(UserNameBuilder name) => _$this._name = name;

  UserGender _gender;
  UserGender get gender => _$this._gender;
  set gender(UserGender gender) => _$this._gender = gender;

  SchoolBuilder _school;
  SchoolBuilder get school => _$this._school ??= new SchoolBuilder();
  set school(SchoolBuilder school) => _$this._school = school;

  String _headshotURL;
  String get headshotURL => _$this._headshotURL;
  set headshotURL(String headshotURL) => _$this._headshotURL = headshotURL;

  DateTime _birth;
  DateTime get birth => _$this._birth;
  set birth(DateTime birth) => _$this._birth = birth;

  UserRelationship _relationship;
  UserRelationship get relationship => _$this._relationship;
  set relationship(UserRelationship relationship) =>
      _$this._relationship = relationship;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  UserBuilder();

  UserBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name?.toBuilder();
      _gender = _$v.gender;
      _school = _$v.school?.toBuilder();
      _headshotURL = _$v.headshotURL;
      _birth = _$v.birth;
      _relationship = _$v.relationship;
      _id = _$v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(User other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$User;
  }

  @override
  void update(void updates(UserBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$User build() {
    _$User _$result;
    try {
      _$result = _$v ??
          new _$User._(
              name: name.build(),
              gender: gender,
              school: school.build(),
              headshotURL: headshotURL,
              birth: birth,
              relationship: relationship,
              id: id);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'name';
        name.build();

        _$failedField = 'school';
        school.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'User', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$UserName extends UserName {
  @override
  final String displayName;
  @override
  final String firstName;
  @override
  final String lastName;

  factory _$UserName([void updates(UserNameBuilder b)]) =>
      (new UserNameBuilder()..update(updates)).build();

  _$UserName._({this.displayName, this.firstName, this.lastName}) : super._() {
    if (displayName == null) {
      throw new BuiltValueNullFieldError('UserName', 'displayName');
    }
    if (firstName == null) {
      throw new BuiltValueNullFieldError('UserName', 'firstName');
    }
    if (lastName == null) {
      throw new BuiltValueNullFieldError('UserName', 'lastName');
    }
  }

  @override
  UserName rebuild(void updates(UserNameBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  UserNameBuilder toBuilder() => new UserNameBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserName &&
        displayName == other.displayName &&
        firstName == other.firstName &&
        lastName == other.lastName;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, displayName.hashCode), firstName.hashCode),
        lastName.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserName')
          ..add('displayName', displayName)
          ..add('firstName', firstName)
          ..add('lastName', lastName))
        .toString();
  }
}

class UserNameBuilder implements Builder<UserName, UserNameBuilder> {
  _$UserName _$v;

  String _displayName;
  String get displayName => _$this._displayName;
  set displayName(String displayName) => _$this._displayName = displayName;

  String _firstName;
  String get firstName => _$this._firstName;
  set firstName(String firstName) => _$this._firstName = firstName;

  String _lastName;
  String get lastName => _$this._lastName;
  set lastName(String lastName) => _$this._lastName = lastName;

  UserNameBuilder();

  UserNameBuilder get _$this {
    if (_$v != null) {
      _displayName = _$v.displayName;
      _firstName = _$v.firstName;
      _lastName = _$v.lastName;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserName other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserName;
  }

  @override
  void update(void updates(UserNameBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$UserName build() {
    final _$result = _$v ??
        new _$UserName._(
            displayName: displayName, firstName: firstName, lastName: lastName);
    replace(_$result);
    return _$result;
  }
}

class _$UserEventConnection extends UserEventConnection {
  @override
  final BuiltList<UserEventEdge> edges;
  @override
  final BuiltList<Event> nodes;
  @override
  final PageInfo pageInfo;
  @override
  final int count;

  factory _$UserEventConnection([void updates(UserEventConnectionBuilder b)]) =>
      (new UserEventConnectionBuilder()..update(updates)).build();

  _$UserEventConnection._({this.edges, this.nodes, this.pageInfo, this.count})
      : super._() {
    if (edges == null) {
      throw new BuiltValueNullFieldError('UserEventConnection', 'edges');
    }
    if (nodes == null) {
      throw new BuiltValueNullFieldError('UserEventConnection', 'nodes');
    }
    if (pageInfo == null) {
      throw new BuiltValueNullFieldError('UserEventConnection', 'pageInfo');
    }
    if (count == null) {
      throw new BuiltValueNullFieldError('UserEventConnection', 'count');
    }
  }

  @override
  UserEventConnection rebuild(void updates(UserEventConnectionBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  UserEventConnectionBuilder toBuilder() =>
      new UserEventConnectionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserEventConnection &&
        edges == other.edges &&
        nodes == other.nodes &&
        pageInfo == other.pageInfo &&
        count == other.count;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, edges.hashCode), nodes.hashCode), pageInfo.hashCode),
        count.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserEventConnection')
          ..add('edges', edges)
          ..add('nodes', nodes)
          ..add('pageInfo', pageInfo)
          ..add('count', count))
        .toString();
  }
}

class UserEventConnectionBuilder
    implements Builder<UserEventConnection, UserEventConnectionBuilder> {
  _$UserEventConnection _$v;

  ListBuilder<UserEventEdge> _edges;
  ListBuilder<UserEventEdge> get edges =>
      _$this._edges ??= new ListBuilder<UserEventEdge>();
  set edges(ListBuilder<UserEventEdge> edges) => _$this._edges = edges;

  ListBuilder<Event> _nodes;
  ListBuilder<Event> get nodes => _$this._nodes ??= new ListBuilder<Event>();
  set nodes(ListBuilder<Event> nodes) => _$this._nodes = nodes;

  PageInfoBuilder _pageInfo;
  PageInfoBuilder get pageInfo => _$this._pageInfo ??= new PageInfoBuilder();
  set pageInfo(PageInfoBuilder pageInfo) => _$this._pageInfo = pageInfo;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  UserEventConnectionBuilder();

  UserEventConnectionBuilder get _$this {
    if (_$v != null) {
      _edges = _$v.edges?.toBuilder();
      _nodes = _$v.nodes?.toBuilder();
      _pageInfo = _$v.pageInfo?.toBuilder();
      _count = _$v.count;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserEventConnection other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserEventConnection;
  }

  @override
  void update(void updates(UserEventConnectionBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$UserEventConnection build() {
    _$UserEventConnection _$result;
    try {
      _$result = _$v ??
          new _$UserEventConnection._(
              edges: edges.build(),
              nodes: nodes.build(),
              pageInfo: pageInfo.build(),
              count: count);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'edges';
        edges.build();
        _$failedField = 'nodes';
        nodes.build();
        _$failedField = 'pageInfo';
        pageInfo.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'UserEventConnection', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$UserEventEdge extends UserEventEdge {
  @override
  final Event node;
  @override
  final DateTime createdAt;
  @override
  final UserEventState state;
  @override
  final String cursor;

  factory _$UserEventEdge([void updates(UserEventEdgeBuilder b)]) =>
      (new UserEventEdgeBuilder()..update(updates)).build();

  _$UserEventEdge._({this.node, this.createdAt, this.state, this.cursor})
      : super._() {
    if (node == null) {
      throw new BuiltValueNullFieldError('UserEventEdge', 'node');
    }
    if (createdAt == null) {
      throw new BuiltValueNullFieldError('UserEventEdge', 'createdAt');
    }
    if (state == null) {
      throw new BuiltValueNullFieldError('UserEventEdge', 'state');
    }
    if (cursor == null) {
      throw new BuiltValueNullFieldError('UserEventEdge', 'cursor');
    }
  }

  @override
  UserEventEdge rebuild(void updates(UserEventEdgeBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  UserEventEdgeBuilder toBuilder() => new UserEventEdgeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserEventEdge &&
        node == other.node &&
        createdAt == other.createdAt &&
        state == other.state &&
        cursor == other.cursor;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, node.hashCode), createdAt.hashCode), state.hashCode),
        cursor.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserEventEdge')
          ..add('node', node)
          ..add('createdAt', createdAt)
          ..add('state', state)
          ..add('cursor', cursor))
        .toString();
  }
}

class UserEventEdgeBuilder
    implements Builder<UserEventEdge, UserEventEdgeBuilder> {
  _$UserEventEdge _$v;

  EventBuilder _node;
  EventBuilder get node => _$this._node ??= new EventBuilder();
  set node(EventBuilder node) => _$this._node = node;

  DateTime _createdAt;
  DateTime get createdAt => _$this._createdAt;
  set createdAt(DateTime createdAt) => _$this._createdAt = createdAt;

  UserEventState _state;
  UserEventState get state => _$this._state;
  set state(UserEventState state) => _$this._state = state;

  String _cursor;
  String get cursor => _$this._cursor;
  set cursor(String cursor) => _$this._cursor = cursor;

  UserEventEdgeBuilder();

  UserEventEdgeBuilder get _$this {
    if (_$v != null) {
      _node = _$v.node?.toBuilder();
      _createdAt = _$v.createdAt;
      _state = _$v.state;
      _cursor = _$v.cursor;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserEventEdge other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserEventEdge;
  }

  @override
  void update(void updates(UserEventEdgeBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$UserEventEdge build() {
    _$UserEventEdge _$result;
    try {
      _$result = _$v ??
          new _$UserEventEdge._(
              node: node.build(),
              createdAt: createdAt,
              state: state,
              cursor: cursor);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'node';
        node.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'UserEventEdge', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$UserFriendConnection extends UserFriendConnection {
  @override
  final BuiltList<UserFriendEdge> edges;
  @override
  final BuiltList<User> nodes;
  @override
  final PageInfo pageInfo;
  @override
  final int count;

  factory _$UserFriendConnection(
          [void updates(UserFriendConnectionBuilder b)]) =>
      (new UserFriendConnectionBuilder()..update(updates)).build();

  _$UserFriendConnection._({this.edges, this.nodes, this.pageInfo, this.count})
      : super._() {
    if (edges == null) {
      throw new BuiltValueNullFieldError('UserFriendConnection', 'edges');
    }
    if (nodes == null) {
      throw new BuiltValueNullFieldError('UserFriendConnection', 'nodes');
    }
    if (pageInfo == null) {
      throw new BuiltValueNullFieldError('UserFriendConnection', 'pageInfo');
    }
    if (count == null) {
      throw new BuiltValueNullFieldError('UserFriendConnection', 'count');
    }
  }

  @override
  UserFriendConnection rebuild(void updates(UserFriendConnectionBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  UserFriendConnectionBuilder toBuilder() =>
      new UserFriendConnectionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserFriendConnection &&
        edges == other.edges &&
        nodes == other.nodes &&
        pageInfo == other.pageInfo &&
        count == other.count;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, edges.hashCode), nodes.hashCode), pageInfo.hashCode),
        count.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserFriendConnection')
          ..add('edges', edges)
          ..add('nodes', nodes)
          ..add('pageInfo', pageInfo)
          ..add('count', count))
        .toString();
  }
}

class UserFriendConnectionBuilder
    implements Builder<UserFriendConnection, UserFriendConnectionBuilder> {
  _$UserFriendConnection _$v;

  ListBuilder<UserFriendEdge> _edges;
  ListBuilder<UserFriendEdge> get edges =>
      _$this._edges ??= new ListBuilder<UserFriendEdge>();
  set edges(ListBuilder<UserFriendEdge> edges) => _$this._edges = edges;

  ListBuilder<User> _nodes;
  ListBuilder<User> get nodes => _$this._nodes ??= new ListBuilder<User>();
  set nodes(ListBuilder<User> nodes) => _$this._nodes = nodes;

  PageInfoBuilder _pageInfo;
  PageInfoBuilder get pageInfo => _$this._pageInfo ??= new PageInfoBuilder();
  set pageInfo(PageInfoBuilder pageInfo) => _$this._pageInfo = pageInfo;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  UserFriendConnectionBuilder();

  UserFriendConnectionBuilder get _$this {
    if (_$v != null) {
      _edges = _$v.edges?.toBuilder();
      _nodes = _$v.nodes?.toBuilder();
      _pageInfo = _$v.pageInfo?.toBuilder();
      _count = _$v.count;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserFriendConnection other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserFriendConnection;
  }

  @override
  void update(void updates(UserFriendConnectionBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$UserFriendConnection build() {
    _$UserFriendConnection _$result;
    try {
      _$result = _$v ??
          new _$UserFriendConnection._(
              edges: edges.build(),
              nodes: nodes.build(),
              pageInfo: pageInfo.build(),
              count: count);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'edges';
        edges.build();
        _$failedField = 'nodes';
        nodes.build();
        _$failedField = 'pageInfo';
        pageInfo.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'UserFriendConnection', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$UserFieldConnection extends UserFieldConnection {
  @override
  final BuiltList<UserFieldEdge> edges;
  @override
  final BuiltList<SchoolField> nodes;
  @override
  final PageInfo pageInfo;
  @override
  final int count;

  factory _$UserFieldConnection([void updates(UserFieldConnectionBuilder b)]) =>
      (new UserFieldConnectionBuilder()..update(updates)).build();

  _$UserFieldConnection._({this.edges, this.nodes, this.pageInfo, this.count})
      : super._() {
    if (edges == null) {
      throw new BuiltValueNullFieldError('UserFieldConnection', 'edges');
    }
    if (nodes == null) {
      throw new BuiltValueNullFieldError('UserFieldConnection', 'nodes');
    }
    if (pageInfo == null) {
      throw new BuiltValueNullFieldError('UserFieldConnection', 'pageInfo');
    }
    if (count == null) {
      throw new BuiltValueNullFieldError('UserFieldConnection', 'count');
    }
  }

  @override
  UserFieldConnection rebuild(void updates(UserFieldConnectionBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  UserFieldConnectionBuilder toBuilder() =>
      new UserFieldConnectionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserFieldConnection &&
        edges == other.edges &&
        nodes == other.nodes &&
        pageInfo == other.pageInfo &&
        count == other.count;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, edges.hashCode), nodes.hashCode), pageInfo.hashCode),
        count.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserFieldConnection')
          ..add('edges', edges)
          ..add('nodes', nodes)
          ..add('pageInfo', pageInfo)
          ..add('count', count))
        .toString();
  }
}

class UserFieldConnectionBuilder
    implements Builder<UserFieldConnection, UserFieldConnectionBuilder> {
  _$UserFieldConnection _$v;

  ListBuilder<UserFieldEdge> _edges;
  ListBuilder<UserFieldEdge> get edges =>
      _$this._edges ??= new ListBuilder<UserFieldEdge>();
  set edges(ListBuilder<UserFieldEdge> edges) => _$this._edges = edges;

  ListBuilder<SchoolField> _nodes;
  ListBuilder<SchoolField> get nodes =>
      _$this._nodes ??= new ListBuilder<SchoolField>();
  set nodes(ListBuilder<SchoolField> nodes) => _$this._nodes = nodes;

  PageInfoBuilder _pageInfo;
  PageInfoBuilder get pageInfo => _$this._pageInfo ??= new PageInfoBuilder();
  set pageInfo(PageInfoBuilder pageInfo) => _$this._pageInfo = pageInfo;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  UserFieldConnectionBuilder();

  UserFieldConnectionBuilder get _$this {
    if (_$v != null) {
      _edges = _$v.edges?.toBuilder();
      _nodes = _$v.nodes?.toBuilder();
      _pageInfo = _$v.pageInfo?.toBuilder();
      _count = _$v.count;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserFieldConnection other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserFieldConnection;
  }

  @override
  void update(void updates(UserFieldConnectionBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$UserFieldConnection build() {
    _$UserFieldConnection _$result;
    try {
      _$result = _$v ??
          new _$UserFieldConnection._(
              edges: edges.build(),
              nodes: nodes.build(),
              pageInfo: pageInfo.build(),
              count: count);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'edges';
        edges.build();
        _$failedField = 'nodes';
        nodes.build();
        _$failedField = 'pageInfo';
        pageInfo.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'UserFieldConnection', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$UserFriendEdge extends UserFriendEdge {
  @override
  final User node;
  @override
  final DateTime createdAt;
  @override
  final String cursor;

  factory _$UserFriendEdge([void updates(UserFriendEdgeBuilder b)]) =>
      (new UserFriendEdgeBuilder()..update(updates)).build();

  _$UserFriendEdge._({this.node, this.createdAt, this.cursor}) : super._() {
    if (node == null) {
      throw new BuiltValueNullFieldError('UserFriendEdge', 'node');
    }
    if (createdAt == null) {
      throw new BuiltValueNullFieldError('UserFriendEdge', 'createdAt');
    }
    if (cursor == null) {
      throw new BuiltValueNullFieldError('UserFriendEdge', 'cursor');
    }
  }

  @override
  UserFriendEdge rebuild(void updates(UserFriendEdgeBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  UserFriendEdgeBuilder toBuilder() =>
      new UserFriendEdgeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserFriendEdge &&
        node == other.node &&
        createdAt == other.createdAt &&
        cursor == other.cursor;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, node.hashCode), createdAt.hashCode), cursor.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserFriendEdge')
          ..add('node', node)
          ..add('createdAt', createdAt)
          ..add('cursor', cursor))
        .toString();
  }
}

class UserFriendEdgeBuilder
    implements Builder<UserFriendEdge, UserFriendEdgeBuilder> {
  _$UserFriendEdge _$v;

  UserBuilder _node;
  UserBuilder get node => _$this._node ??= new UserBuilder();
  set node(UserBuilder node) => _$this._node = node;

  DateTime _createdAt;
  DateTime get createdAt => _$this._createdAt;
  set createdAt(DateTime createdAt) => _$this._createdAt = createdAt;

  String _cursor;
  String get cursor => _$this._cursor;
  set cursor(String cursor) => _$this._cursor = cursor;

  UserFriendEdgeBuilder();

  UserFriendEdgeBuilder get _$this {
    if (_$v != null) {
      _node = _$v.node?.toBuilder();
      _createdAt = _$v.createdAt;
      _cursor = _$v.cursor;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserFriendEdge other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserFriendEdge;
  }

  @override
  void update(void updates(UserFriendEdgeBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$UserFriendEdge build() {
    _$UserFriendEdge _$result;
    try {
      _$result = _$v ??
          new _$UserFriendEdge._(
              node: node.build(), createdAt: createdAt, cursor: cursor);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'node';
        node.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'UserFriendEdge', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$UserFieldEdge extends UserFieldEdge {
  @override
  final SchoolField node;
  @override
  final UserFieldState state;
  @override
  final String cursor;

  factory _$UserFieldEdge([void updates(UserFieldEdgeBuilder b)]) =>
      (new UserFieldEdgeBuilder()..update(updates)).build();

  _$UserFieldEdge._({this.node, this.state, this.cursor}) : super._() {
    if (node == null) {
      throw new BuiltValueNullFieldError('UserFieldEdge', 'node');
    }
    if (state == null) {
      throw new BuiltValueNullFieldError('UserFieldEdge', 'state');
    }
    if (cursor == null) {
      throw new BuiltValueNullFieldError('UserFieldEdge', 'cursor');
    }
  }

  @override
  UserFieldEdge rebuild(void updates(UserFieldEdgeBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  UserFieldEdgeBuilder toBuilder() => new UserFieldEdgeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserFieldEdge &&
        node == other.node &&
        state == other.state &&
        cursor == other.cursor;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, node.hashCode), state.hashCode), cursor.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserFieldEdge')
          ..add('node', node)
          ..add('state', state)
          ..add('cursor', cursor))
        .toString();
  }
}

class UserFieldEdgeBuilder
    implements Builder<UserFieldEdge, UserFieldEdgeBuilder> {
  _$UserFieldEdge _$v;

  SchoolFieldBuilder _node;
  SchoolFieldBuilder get node => _$this._node ??= new SchoolFieldBuilder();
  set node(SchoolFieldBuilder node) => _$this._node = node;

  UserFieldState _state;
  UserFieldState get state => _$this._state;
  set state(UserFieldState state) => _$this._state = state;

  String _cursor;
  String get cursor => _$this._cursor;
  set cursor(String cursor) => _$this._cursor = cursor;

  UserFieldEdgeBuilder();

  UserFieldEdgeBuilder get _$this {
    if (_$v != null) {
      _node = _$v.node?.toBuilder();
      _state = _$v.state;
      _cursor = _$v.cursor;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserFieldEdge other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserFieldEdge;
  }

  @override
  void update(void updates(UserFieldEdgeBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$UserFieldEdge build() {
    _$UserFieldEdge _$result;
    try {
      _$result = _$v ??
          new _$UserFieldEdge._(
              node: node.build(), state: state, cursor: cursor);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'node';
        node.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'UserFieldEdge', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$School extends School {
  @override
  final String name;
  @override
  final String id;

  factory _$School([void updates(SchoolBuilder b)]) =>
      (new SchoolBuilder()..update(updates)).build();

  _$School._({this.name, this.id}) : super._() {
    if (name == null) {
      throw new BuiltValueNullFieldError('School', 'name');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('School', 'id');
    }
  }

  @override
  School rebuild(void updates(SchoolBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  SchoolBuilder toBuilder() => new SchoolBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is School && name == other.name && id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, name.hashCode), id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('School')
          ..add('name', name)
          ..add('id', id))
        .toString();
  }
}

class SchoolBuilder implements Builder<School, SchoolBuilder> {
  _$School _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  SchoolBuilder();

  SchoolBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _id = _$v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(School other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$School;
  }

  @override
  void update(void updates(SchoolBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$School build() {
    final _$result = _$v ?? new _$School._(name: name, id: id);
    replace(_$result);
    return _$result;
  }
}

class _$SchoolField extends SchoolField {
  @override
  final School school;
  @override
  final String name;
  @override
  final BuiltList<StudyField> fields;
  @override
  final String id;

  factory _$SchoolField([void updates(SchoolFieldBuilder b)]) =>
      (new SchoolFieldBuilder()..update(updates)).build();

  _$SchoolField._({this.school, this.name, this.fields, this.id}) : super._() {
    if (school == null) {
      throw new BuiltValueNullFieldError('SchoolField', 'school');
    }
    if (name == null) {
      throw new BuiltValueNullFieldError('SchoolField', 'name');
    }
    if (fields == null) {
      throw new BuiltValueNullFieldError('SchoolField', 'fields');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('SchoolField', 'id');
    }
  }

  @override
  SchoolField rebuild(void updates(SchoolFieldBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  SchoolFieldBuilder toBuilder() => new SchoolFieldBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SchoolField &&
        school == other.school &&
        name == other.name &&
        fields == other.fields &&
        id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, school.hashCode), name.hashCode), fields.hashCode),
        id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SchoolField')
          ..add('school', school)
          ..add('name', name)
          ..add('fields', fields)
          ..add('id', id))
        .toString();
  }
}

class SchoolFieldBuilder implements Builder<SchoolField, SchoolFieldBuilder> {
  _$SchoolField _$v;

  SchoolBuilder _school;
  SchoolBuilder get school => _$this._school ??= new SchoolBuilder();
  set school(SchoolBuilder school) => _$this._school = school;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  ListBuilder<StudyField> _fields;
  ListBuilder<StudyField> get fields =>
      _$this._fields ??= new ListBuilder<StudyField>();
  set fields(ListBuilder<StudyField> fields) => _$this._fields = fields;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  SchoolFieldBuilder();

  SchoolFieldBuilder get _$this {
    if (_$v != null) {
      _school = _$v.school?.toBuilder();
      _name = _$v.name;
      _fields = _$v.fields?.toBuilder();
      _id = _$v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SchoolField other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SchoolField;
  }

  @override
  void update(void updates(SchoolFieldBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$SchoolField build() {
    _$SchoolField _$result;
    try {
      _$result = _$v ??
          new _$SchoolField._(
              school: school.build(),
              name: name,
              fields: fields.build(),
              id: id);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'school';
        school.build();

        _$failedField = 'fields';
        fields.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'SchoolField', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$SchoolFieldConnection extends SchoolFieldConnection {
  @override
  final BuiltList<SchoolFieldEdge> edges;
  @override
  final BuiltList<SchoolField> nodes;
  @override
  final PageInfo pageInfo;
  @override
  final int count;

  factory _$SchoolFieldConnection(
          [void updates(SchoolFieldConnectionBuilder b)]) =>
      (new SchoolFieldConnectionBuilder()..update(updates)).build();

  _$SchoolFieldConnection._({this.edges, this.nodes, this.pageInfo, this.count})
      : super._() {
    if (edges == null) {
      throw new BuiltValueNullFieldError('SchoolFieldConnection', 'edges');
    }
    if (nodes == null) {
      throw new BuiltValueNullFieldError('SchoolFieldConnection', 'nodes');
    }
    if (pageInfo == null) {
      throw new BuiltValueNullFieldError('SchoolFieldConnection', 'pageInfo');
    }
    if (count == null) {
      throw new BuiltValueNullFieldError('SchoolFieldConnection', 'count');
    }
  }

  @override
  SchoolFieldConnection rebuild(void updates(SchoolFieldConnectionBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  SchoolFieldConnectionBuilder toBuilder() =>
      new SchoolFieldConnectionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SchoolFieldConnection &&
        edges == other.edges &&
        nodes == other.nodes &&
        pageInfo == other.pageInfo &&
        count == other.count;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, edges.hashCode), nodes.hashCode), pageInfo.hashCode),
        count.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SchoolFieldConnection')
          ..add('edges', edges)
          ..add('nodes', nodes)
          ..add('pageInfo', pageInfo)
          ..add('count', count))
        .toString();
  }
}

class SchoolFieldConnectionBuilder
    implements Builder<SchoolFieldConnection, SchoolFieldConnectionBuilder> {
  _$SchoolFieldConnection _$v;

  ListBuilder<SchoolFieldEdge> _edges;
  ListBuilder<SchoolFieldEdge> get edges =>
      _$this._edges ??= new ListBuilder<SchoolFieldEdge>();
  set edges(ListBuilder<SchoolFieldEdge> edges) => _$this._edges = edges;

  ListBuilder<SchoolField> _nodes;
  ListBuilder<SchoolField> get nodes =>
      _$this._nodes ??= new ListBuilder<SchoolField>();
  set nodes(ListBuilder<SchoolField> nodes) => _$this._nodes = nodes;

  PageInfoBuilder _pageInfo;
  PageInfoBuilder get pageInfo => _$this._pageInfo ??= new PageInfoBuilder();
  set pageInfo(PageInfoBuilder pageInfo) => _$this._pageInfo = pageInfo;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  SchoolFieldConnectionBuilder();

  SchoolFieldConnectionBuilder get _$this {
    if (_$v != null) {
      _edges = _$v.edges?.toBuilder();
      _nodes = _$v.nodes?.toBuilder();
      _pageInfo = _$v.pageInfo?.toBuilder();
      _count = _$v.count;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SchoolFieldConnection other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SchoolFieldConnection;
  }

  @override
  void update(void updates(SchoolFieldConnectionBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$SchoolFieldConnection build() {
    _$SchoolFieldConnection _$result;
    try {
      _$result = _$v ??
          new _$SchoolFieldConnection._(
              edges: edges.build(),
              nodes: nodes.build(),
              pageInfo: pageInfo.build(),
              count: count);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'edges';
        edges.build();
        _$failedField = 'nodes';
        nodes.build();
        _$failedField = 'pageInfo';
        pageInfo.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'SchoolFieldConnection', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$SchoolFieldEdge extends SchoolFieldEdge {
  @override
  final SchoolField node;
  @override
  final String cursor;

  factory _$SchoolFieldEdge([void updates(SchoolFieldEdgeBuilder b)]) =>
      (new SchoolFieldEdgeBuilder()..update(updates)).build();

  _$SchoolFieldEdge._({this.node, this.cursor}) : super._() {
    if (node == null) {
      throw new BuiltValueNullFieldError('SchoolFieldEdge', 'node');
    }
    if (cursor == null) {
      throw new BuiltValueNullFieldError('SchoolFieldEdge', 'cursor');
    }
  }

  @override
  SchoolFieldEdge rebuild(void updates(SchoolFieldEdgeBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  SchoolFieldEdgeBuilder toBuilder() =>
      new SchoolFieldEdgeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SchoolFieldEdge &&
        node == other.node &&
        cursor == other.cursor;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, node.hashCode), cursor.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SchoolFieldEdge')
          ..add('node', node)
          ..add('cursor', cursor))
        .toString();
  }
}

class SchoolFieldEdgeBuilder
    implements Builder<SchoolFieldEdge, SchoolFieldEdgeBuilder> {
  _$SchoolFieldEdge _$v;

  SchoolFieldBuilder _node;
  SchoolFieldBuilder get node => _$this._node ??= new SchoolFieldBuilder();
  set node(SchoolFieldBuilder node) => _$this._node = node;

  String _cursor;
  String get cursor => _$this._cursor;
  set cursor(String cursor) => _$this._cursor = cursor;

  SchoolFieldEdgeBuilder();

  SchoolFieldEdgeBuilder get _$this {
    if (_$v != null) {
      _node = _$v.node?.toBuilder();
      _cursor = _$v.cursor;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SchoolFieldEdge other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SchoolFieldEdge;
  }

  @override
  void update(void updates(SchoolFieldEdgeBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$SchoolFieldEdge build() {
    _$SchoolFieldEdge _$result;
    try {
      _$result =
          _$v ?? new _$SchoolFieldEdge._(node: node.build(), cursor: cursor);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'node';
        node.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'SchoolFieldEdge', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Event extends Event {
  @override
  final String name;
  @override
  final String description;
  @override
  final DateTimePeriod hostDate;
  @override
  final EventChannel channel;
  @override
  final EventState state;
  @override
  final UserEventState viewerState;
  @override
  final bool viewerCanJoin;
  @override
  final bool viewerCanFollow;
  @override
  final String id;

  factory _$Event([void updates(EventBuilder b)]) =>
      (new EventBuilder()..update(updates)).build();

  _$Event._(
      {this.name,
      this.description,
      this.hostDate,
      this.channel,
      this.state,
      this.viewerState,
      this.viewerCanJoin,
      this.viewerCanFollow,
      this.id})
      : super._() {
    if (name == null) {
      throw new BuiltValueNullFieldError('Event', 'name');
    }
    if (description == null) {
      throw new BuiltValueNullFieldError('Event', 'description');
    }
    if (hostDate == null) {
      throw new BuiltValueNullFieldError('Event', 'hostDate');
    }
    if (channel == null) {
      throw new BuiltValueNullFieldError('Event', 'channel');
    }
    if (state == null) {
      throw new BuiltValueNullFieldError('Event', 'state');
    }
    if (viewerState == null) {
      throw new BuiltValueNullFieldError('Event', 'viewerState');
    }
    if (viewerCanJoin == null) {
      throw new BuiltValueNullFieldError('Event', 'viewerCanJoin');
    }
    if (viewerCanFollow == null) {
      throw new BuiltValueNullFieldError('Event', 'viewerCanFollow');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('Event', 'id');
    }
  }

  @override
  Event rebuild(void updates(EventBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  EventBuilder toBuilder() => new EventBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Event &&
        name == other.name &&
        description == other.description &&
        hostDate == other.hostDate &&
        channel == other.channel &&
        state == other.state &&
        viewerState == other.viewerState &&
        viewerCanJoin == other.viewerCanJoin &&
        viewerCanFollow == other.viewerCanFollow &&
        id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc($jc(0, name.hashCode),
                                    description.hashCode),
                                hostDate.hashCode),
                            channel.hashCode),
                        state.hashCode),
                    viewerState.hashCode),
                viewerCanJoin.hashCode),
            viewerCanFollow.hashCode),
        id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Event')
          ..add('name', name)
          ..add('description', description)
          ..add('hostDate', hostDate)
          ..add('channel', channel)
          ..add('state', state)
          ..add('viewerState', viewerState)
          ..add('viewerCanJoin', viewerCanJoin)
          ..add('viewerCanFollow', viewerCanFollow)
          ..add('id', id))
        .toString();
  }
}

class EventBuilder implements Builder<Event, EventBuilder> {
  _$Event _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  DateTimePeriodBuilder _hostDate;
  DateTimePeriodBuilder get hostDate =>
      _$this._hostDate ??= new DateTimePeriodBuilder();
  set hostDate(DateTimePeriodBuilder hostDate) => _$this._hostDate = hostDate;

  EventChannelBuilder _channel;
  EventChannelBuilder get channel =>
      _$this._channel ??= new EventChannelBuilder();
  set channel(EventChannelBuilder channel) => _$this._channel = channel;

  EventState _state;
  EventState get state => _$this._state;
  set state(EventState state) => _$this._state = state;

  UserEventState _viewerState;
  UserEventState get viewerState => _$this._viewerState;
  set viewerState(UserEventState viewerState) =>
      _$this._viewerState = viewerState;

  bool _viewerCanJoin;
  bool get viewerCanJoin => _$this._viewerCanJoin;
  set viewerCanJoin(bool viewerCanJoin) =>
      _$this._viewerCanJoin = viewerCanJoin;

  bool _viewerCanFollow;
  bool get viewerCanFollow => _$this._viewerCanFollow;
  set viewerCanFollow(bool viewerCanFollow) =>
      _$this._viewerCanFollow = viewerCanFollow;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  EventBuilder();

  EventBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _description = _$v.description;
      _hostDate = _$v.hostDate?.toBuilder();
      _channel = _$v.channel?.toBuilder();
      _state = _$v.state;
      _viewerState = _$v.viewerState;
      _viewerCanJoin = _$v.viewerCanJoin;
      _viewerCanFollow = _$v.viewerCanFollow;
      _id = _$v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Event other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Event;
  }

  @override
  void update(void updates(EventBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Event build() {
    _$Event _$result;
    try {
      _$result = _$v ??
          new _$Event._(
              name: name,
              description: description,
              hostDate: hostDate.build(),
              channel: channel.build(),
              state: state,
              viewerState: viewerState,
              viewerCanJoin: viewerCanJoin,
              viewerCanFollow: viewerCanFollow,
              id: id);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'hostDate';
        hostDate.build();
        _$failedField = 'channel';
        channel.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Event', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$EventHostConnection extends EventHostConnection {
  @override
  final BuiltList<EventHostEdge> edges;
  @override
  final BuiltList<Actor> nodes;
  @override
  final PageInfo pageInfo;
  @override
  final int count;

  factory _$EventHostConnection([void updates(EventHostConnectionBuilder b)]) =>
      (new EventHostConnectionBuilder()..update(updates)).build();

  _$EventHostConnection._({this.edges, this.nodes, this.pageInfo, this.count})
      : super._() {
    if (edges == null) {
      throw new BuiltValueNullFieldError('EventHostConnection', 'edges');
    }
    if (nodes == null) {
      throw new BuiltValueNullFieldError('EventHostConnection', 'nodes');
    }
    if (pageInfo == null) {
      throw new BuiltValueNullFieldError('EventHostConnection', 'pageInfo');
    }
    if (count == null) {
      throw new BuiltValueNullFieldError('EventHostConnection', 'count');
    }
  }

  @override
  EventHostConnection rebuild(void updates(EventHostConnectionBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  EventHostConnectionBuilder toBuilder() =>
      new EventHostConnectionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EventHostConnection &&
        edges == other.edges &&
        nodes == other.nodes &&
        pageInfo == other.pageInfo &&
        count == other.count;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, edges.hashCode), nodes.hashCode), pageInfo.hashCode),
        count.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('EventHostConnection')
          ..add('edges', edges)
          ..add('nodes', nodes)
          ..add('pageInfo', pageInfo)
          ..add('count', count))
        .toString();
  }
}

class EventHostConnectionBuilder
    implements Builder<EventHostConnection, EventHostConnectionBuilder> {
  _$EventHostConnection _$v;

  ListBuilder<EventHostEdge> _edges;
  ListBuilder<EventHostEdge> get edges =>
      _$this._edges ??= new ListBuilder<EventHostEdge>();
  set edges(ListBuilder<EventHostEdge> edges) => _$this._edges = edges;

  ListBuilder<Actor> _nodes;
  ListBuilder<Actor> get nodes => _$this._nodes ??= new ListBuilder<Actor>();
  set nodes(ListBuilder<Actor> nodes) => _$this._nodes = nodes;

  PageInfoBuilder _pageInfo;
  PageInfoBuilder get pageInfo => _$this._pageInfo ??= new PageInfoBuilder();
  set pageInfo(PageInfoBuilder pageInfo) => _$this._pageInfo = pageInfo;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  EventHostConnectionBuilder();

  EventHostConnectionBuilder get _$this {
    if (_$v != null) {
      _edges = _$v.edges?.toBuilder();
      _nodes = _$v.nodes?.toBuilder();
      _pageInfo = _$v.pageInfo?.toBuilder();
      _count = _$v.count;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EventHostConnection other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$EventHostConnection;
  }

  @override
  void update(void updates(EventHostConnectionBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$EventHostConnection build() {
    _$EventHostConnection _$result;
    try {
      _$result = _$v ??
          new _$EventHostConnection._(
              edges: edges.build(),
              nodes: nodes.build(),
              pageInfo: pageInfo.build(),
              count: count);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'edges';
        edges.build();
        _$failedField = 'nodes';
        nodes.build();
        _$failedField = 'pageInfo';
        pageInfo.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'EventHostConnection', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$EventHostEdge extends EventHostEdge {
  @override
  final Actor node;
  @override
  final String cursor;

  factory _$EventHostEdge([void updates(EventHostEdgeBuilder b)]) =>
      (new EventHostEdgeBuilder()..update(updates)).build();

  _$EventHostEdge._({this.node, this.cursor}) : super._() {
    if (node == null) {
      throw new BuiltValueNullFieldError('EventHostEdge', 'node');
    }
    if (cursor == null) {
      throw new BuiltValueNullFieldError('EventHostEdge', 'cursor');
    }
  }

  @override
  EventHostEdge rebuild(void updates(EventHostEdgeBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  EventHostEdgeBuilder toBuilder() => new EventHostEdgeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EventHostEdge &&
        node == other.node &&
        cursor == other.cursor;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, node.hashCode), cursor.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('EventHostEdge')
          ..add('node', node)
          ..add('cursor', cursor))
        .toString();
  }
}

class EventHostEdgeBuilder
    implements Builder<EventHostEdge, EventHostEdgeBuilder> {
  _$EventHostEdge _$v;

  Actor _node;
  Actor get node => _$this._node;
  set node(Actor node) => _$this._node = node;

  String _cursor;
  String get cursor => _$this._cursor;
  set cursor(String cursor) => _$this._cursor = cursor;

  EventHostEdgeBuilder();

  EventHostEdgeBuilder get _$this {
    if (_$v != null) {
      _node = _$v.node;
      _cursor = _$v.cursor;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EventHostEdge other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$EventHostEdge;
  }

  @override
  void update(void updates(EventHostEdgeBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$EventHostEdge build() {
    final _$result = _$v ?? new _$EventHostEdge._(node: node, cursor: cursor);
    replace(_$result);
    return _$result;
  }
}

class _$EventFriendConnection extends EventFriendConnection {
  @override
  final BuiltList<EventFriendEdge> edges;
  @override
  final BuiltList<User> nodes;
  @override
  final PageInfo pageInfo;
  @override
  final int count;

  factory _$EventFriendConnection(
          [void updates(EventFriendConnectionBuilder b)]) =>
      (new EventFriendConnectionBuilder()..update(updates)).build();

  _$EventFriendConnection._({this.edges, this.nodes, this.pageInfo, this.count})
      : super._() {
    if (edges == null) {
      throw new BuiltValueNullFieldError('EventFriendConnection', 'edges');
    }
    if (nodes == null) {
      throw new BuiltValueNullFieldError('EventFriendConnection', 'nodes');
    }
    if (pageInfo == null) {
      throw new BuiltValueNullFieldError('EventFriendConnection', 'pageInfo');
    }
    if (count == null) {
      throw new BuiltValueNullFieldError('EventFriendConnection', 'count');
    }
  }

  @override
  EventFriendConnection rebuild(void updates(EventFriendConnectionBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  EventFriendConnectionBuilder toBuilder() =>
      new EventFriendConnectionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EventFriendConnection &&
        edges == other.edges &&
        nodes == other.nodes &&
        pageInfo == other.pageInfo &&
        count == other.count;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, edges.hashCode), nodes.hashCode), pageInfo.hashCode),
        count.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('EventFriendConnection')
          ..add('edges', edges)
          ..add('nodes', nodes)
          ..add('pageInfo', pageInfo)
          ..add('count', count))
        .toString();
  }
}

class EventFriendConnectionBuilder
    implements Builder<EventFriendConnection, EventFriendConnectionBuilder> {
  _$EventFriendConnection _$v;

  ListBuilder<EventFriendEdge> _edges;
  ListBuilder<EventFriendEdge> get edges =>
      _$this._edges ??= new ListBuilder<EventFriendEdge>();
  set edges(ListBuilder<EventFriendEdge> edges) => _$this._edges = edges;

  ListBuilder<User> _nodes;
  ListBuilder<User> get nodes => _$this._nodes ??= new ListBuilder<User>();
  set nodes(ListBuilder<User> nodes) => _$this._nodes = nodes;

  PageInfoBuilder _pageInfo;
  PageInfoBuilder get pageInfo => _$this._pageInfo ??= new PageInfoBuilder();
  set pageInfo(PageInfoBuilder pageInfo) => _$this._pageInfo = pageInfo;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  EventFriendConnectionBuilder();

  EventFriendConnectionBuilder get _$this {
    if (_$v != null) {
      _edges = _$v.edges?.toBuilder();
      _nodes = _$v.nodes?.toBuilder();
      _pageInfo = _$v.pageInfo?.toBuilder();
      _count = _$v.count;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EventFriendConnection other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$EventFriendConnection;
  }

  @override
  void update(void updates(EventFriendConnectionBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$EventFriendConnection build() {
    _$EventFriendConnection _$result;
    try {
      _$result = _$v ??
          new _$EventFriendConnection._(
              edges: edges.build(),
              nodes: nodes.build(),
              pageInfo: pageInfo.build(),
              count: count);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'edges';
        edges.build();
        _$failedField = 'nodes';
        nodes.build();
        _$failedField = 'pageInfo';
        pageInfo.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'EventFriendConnection', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$EventFriendEdge extends EventFriendEdge {
  @override
  final User node;
  @override
  final UserEventState state;
  @override
  final String cursor;

  factory _$EventFriendEdge([void updates(EventFriendEdgeBuilder b)]) =>
      (new EventFriendEdgeBuilder()..update(updates)).build();

  _$EventFriendEdge._({this.node, this.state, this.cursor}) : super._() {
    if (node == null) {
      throw new BuiltValueNullFieldError('EventFriendEdge', 'node');
    }
    if (state == null) {
      throw new BuiltValueNullFieldError('EventFriendEdge', 'state');
    }
    if (cursor == null) {
      throw new BuiltValueNullFieldError('EventFriendEdge', 'cursor');
    }
  }

  @override
  EventFriendEdge rebuild(void updates(EventFriendEdgeBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  EventFriendEdgeBuilder toBuilder() =>
      new EventFriendEdgeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EventFriendEdge &&
        node == other.node &&
        state == other.state &&
        cursor == other.cursor;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, node.hashCode), state.hashCode), cursor.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('EventFriendEdge')
          ..add('node', node)
          ..add('state', state)
          ..add('cursor', cursor))
        .toString();
  }
}

class EventFriendEdgeBuilder
    implements Builder<EventFriendEdge, EventFriendEdgeBuilder> {
  _$EventFriendEdge _$v;

  UserBuilder _node;
  UserBuilder get node => _$this._node ??= new UserBuilder();
  set node(UserBuilder node) => _$this._node = node;

  UserEventState _state;
  UserEventState get state => _$this._state;
  set state(UserEventState state) => _$this._state = state;

  String _cursor;
  String get cursor => _$this._cursor;
  set cursor(String cursor) => _$this._cursor = cursor;

  EventFriendEdgeBuilder();

  EventFriendEdgeBuilder get _$this {
    if (_$v != null) {
      _node = _$v.node?.toBuilder();
      _state = _$v.state;
      _cursor = _$v.cursor;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EventFriendEdge other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$EventFriendEdge;
  }

  @override
  void update(void updates(EventFriendEdgeBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$EventFriendEdge build() {
    _$EventFriendEdge _$result;
    try {
      _$result = _$v ??
          new _$EventFriendEdge._(
              node: node.build(), state: state, cursor: cursor);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'node';
        node.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'EventFriendEdge', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$EventChannel extends EventChannel {
  @override
  final String coverUrl;
  @override
  final bool viewerCanAdminister;
  @override
  final bool viewerCanChat;
  @override
  final bool viewerIsMember;
  @override
  final String name;
  @override
  final DateTime viewerLastReadAt;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;
  @override
  final String id;

  factory _$EventChannel([void updates(EventChannelBuilder b)]) =>
      (new EventChannelBuilder()..update(updates)).build();

  _$EventChannel._(
      {this.coverUrl,
      this.viewerCanAdminister,
      this.viewerCanChat,
      this.viewerIsMember,
      this.name,
      this.viewerLastReadAt,
      this.createdAt,
      this.updatedAt,
      this.id})
      : super._() {
    if (coverUrl == null) {
      throw new BuiltValueNullFieldError('EventChannel', 'coverUrl');
    }
    if (viewerCanAdminister == null) {
      throw new BuiltValueNullFieldError('EventChannel', 'viewerCanAdminister');
    }
    if (viewerCanChat == null) {
      throw new BuiltValueNullFieldError('EventChannel', 'viewerCanChat');
    }
    if (viewerIsMember == null) {
      throw new BuiltValueNullFieldError('EventChannel', 'viewerIsMember');
    }
    if (name == null) {
      throw new BuiltValueNullFieldError('EventChannel', 'name');
    }
    if (viewerLastReadAt == null) {
      throw new BuiltValueNullFieldError('EventChannel', 'viewerLastReadAt');
    }
    if (createdAt == null) {
      throw new BuiltValueNullFieldError('EventChannel', 'createdAt');
    }
    if (updatedAt == null) {
      throw new BuiltValueNullFieldError('EventChannel', 'updatedAt');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('EventChannel', 'id');
    }
  }

  @override
  EventChannel rebuild(void updates(EventChannelBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  EventChannelBuilder toBuilder() => new EventChannelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EventChannel &&
        coverUrl == other.coverUrl &&
        viewerCanAdminister == other.viewerCanAdminister &&
        viewerCanChat == other.viewerCanChat &&
        viewerIsMember == other.viewerIsMember &&
        name == other.name &&
        viewerLastReadAt == other.viewerLastReadAt &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt &&
        id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc($jc(0, coverUrl.hashCode),
                                    viewerCanAdminister.hashCode),
                                viewerCanChat.hashCode),
                            viewerIsMember.hashCode),
                        name.hashCode),
                    viewerLastReadAt.hashCode),
                createdAt.hashCode),
            updatedAt.hashCode),
        id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('EventChannel')
          ..add('coverUrl', coverUrl)
          ..add('viewerCanAdminister', viewerCanAdminister)
          ..add('viewerCanChat', viewerCanChat)
          ..add('viewerIsMember', viewerIsMember)
          ..add('name', name)
          ..add('viewerLastReadAt', viewerLastReadAt)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt)
          ..add('id', id))
        .toString();
  }
}

class EventChannelBuilder
    implements Builder<EventChannel, EventChannelBuilder> {
  _$EventChannel _$v;

  String _coverUrl;
  String get coverUrl => _$this._coverUrl;
  set coverUrl(String coverUrl) => _$this._coverUrl = coverUrl;

  bool _viewerCanAdminister;
  bool get viewerCanAdminister => _$this._viewerCanAdminister;
  set viewerCanAdminister(bool viewerCanAdminister) =>
      _$this._viewerCanAdminister = viewerCanAdminister;

  bool _viewerCanChat;
  bool get viewerCanChat => _$this._viewerCanChat;
  set viewerCanChat(bool viewerCanChat) =>
      _$this._viewerCanChat = viewerCanChat;

  bool _viewerIsMember;
  bool get viewerIsMember => _$this._viewerIsMember;
  set viewerIsMember(bool viewerIsMember) =>
      _$this._viewerIsMember = viewerIsMember;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  DateTime _viewerLastReadAt;
  DateTime get viewerLastReadAt => _$this._viewerLastReadAt;
  set viewerLastReadAt(DateTime viewerLastReadAt) =>
      _$this._viewerLastReadAt = viewerLastReadAt;

  DateTime _createdAt;
  DateTime get createdAt => _$this._createdAt;
  set createdAt(DateTime createdAt) => _$this._createdAt = createdAt;

  DateTime _updatedAt;
  DateTime get updatedAt => _$this._updatedAt;
  set updatedAt(DateTime updatedAt) => _$this._updatedAt = updatedAt;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  EventChannelBuilder();

  EventChannelBuilder get _$this {
    if (_$v != null) {
      _coverUrl = _$v.coverUrl;
      _viewerCanAdminister = _$v.viewerCanAdminister;
      _viewerCanChat = _$v.viewerCanChat;
      _viewerIsMember = _$v.viewerIsMember;
      _name = _$v.name;
      _viewerLastReadAt = _$v.viewerLastReadAt;
      _createdAt = _$v.createdAt;
      _updatedAt = _$v.updatedAt;
      _id = _$v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EventChannel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$EventChannel;
  }

  @override
  void update(void updates(EventChannelBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$EventChannel build() {
    final _$result = _$v ??
        new _$EventChannel._(
            coverUrl: coverUrl,
            viewerCanAdminister: viewerCanAdminister,
            viewerCanChat: viewerCanChat,
            viewerIsMember: viewerIsMember,
            name: name,
            viewerLastReadAt: viewerLastReadAt,
            createdAt: createdAt,
            updatedAt: updatedAt,
            id: id);
    replace(_$result);
    return _$result;
  }
}

class _$EventChannelMemberConnection extends EventChannelMemberConnection {
  @override
  final BuiltList<EventChannelMemberEdge> edges;
  @override
  final BuiltList<Actor> nodes;
  @override
  final PageInfo pageInfo;
  @override
  final int count;

  factory _$EventChannelMemberConnection(
          [void updates(EventChannelMemberConnectionBuilder b)]) =>
      (new EventChannelMemberConnectionBuilder()..update(updates)).build();

  _$EventChannelMemberConnection._(
      {this.edges, this.nodes, this.pageInfo, this.count})
      : super._() {
    if (edges == null) {
      throw new BuiltValueNullFieldError(
          'EventChannelMemberConnection', 'edges');
    }
    if (nodes == null) {
      throw new BuiltValueNullFieldError(
          'EventChannelMemberConnection', 'nodes');
    }
    if (pageInfo == null) {
      throw new BuiltValueNullFieldError(
          'EventChannelMemberConnection', 'pageInfo');
    }
    if (count == null) {
      throw new BuiltValueNullFieldError(
          'EventChannelMemberConnection', 'count');
    }
  }

  @override
  EventChannelMemberConnection rebuild(
          void updates(EventChannelMemberConnectionBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  EventChannelMemberConnectionBuilder toBuilder() =>
      new EventChannelMemberConnectionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EventChannelMemberConnection &&
        edges == other.edges &&
        nodes == other.nodes &&
        pageInfo == other.pageInfo &&
        count == other.count;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, edges.hashCode), nodes.hashCode), pageInfo.hashCode),
        count.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('EventChannelMemberConnection')
          ..add('edges', edges)
          ..add('nodes', nodes)
          ..add('pageInfo', pageInfo)
          ..add('count', count))
        .toString();
  }
}

class EventChannelMemberConnectionBuilder
    implements
        Builder<EventChannelMemberConnection,
            EventChannelMemberConnectionBuilder> {
  _$EventChannelMemberConnection _$v;

  ListBuilder<EventChannelMemberEdge> _edges;
  ListBuilder<EventChannelMemberEdge> get edges =>
      _$this._edges ??= new ListBuilder<EventChannelMemberEdge>();
  set edges(ListBuilder<EventChannelMemberEdge> edges) => _$this._edges = edges;

  ListBuilder<Actor> _nodes;
  ListBuilder<Actor> get nodes => _$this._nodes ??= new ListBuilder<Actor>();
  set nodes(ListBuilder<Actor> nodes) => _$this._nodes = nodes;

  PageInfoBuilder _pageInfo;
  PageInfoBuilder get pageInfo => _$this._pageInfo ??= new PageInfoBuilder();
  set pageInfo(PageInfoBuilder pageInfo) => _$this._pageInfo = pageInfo;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  EventChannelMemberConnectionBuilder();

  EventChannelMemberConnectionBuilder get _$this {
    if (_$v != null) {
      _edges = _$v.edges?.toBuilder();
      _nodes = _$v.nodes?.toBuilder();
      _pageInfo = _$v.pageInfo?.toBuilder();
      _count = _$v.count;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EventChannelMemberConnection other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$EventChannelMemberConnection;
  }

  @override
  void update(void updates(EventChannelMemberConnectionBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$EventChannelMemberConnection build() {
    _$EventChannelMemberConnection _$result;
    try {
      _$result = _$v ??
          new _$EventChannelMemberConnection._(
              edges: edges.build(),
              nodes: nodes.build(),
              pageInfo: pageInfo.build(),
              count: count);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'edges';
        edges.build();
        _$failedField = 'nodes';
        nodes.build();
        _$failedField = 'pageInfo';
        pageInfo.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'EventChannelMemberConnection', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$EventChannelMemberEdge extends EventChannelMemberEdge {
  @override
  final Actor node;
  @override
  final UserEventState state;
  @override
  final DateTime createdAt;
  @override
  final String cursor;

  factory _$EventChannelMemberEdge(
          [void updates(EventChannelMemberEdgeBuilder b)]) =>
      (new EventChannelMemberEdgeBuilder()..update(updates)).build();

  _$EventChannelMemberEdge._(
      {this.node, this.state, this.createdAt, this.cursor})
      : super._() {
    if (node == null) {
      throw new BuiltValueNullFieldError('EventChannelMemberEdge', 'node');
    }
    if (state == null) {
      throw new BuiltValueNullFieldError('EventChannelMemberEdge', 'state');
    }
    if (createdAt == null) {
      throw new BuiltValueNullFieldError('EventChannelMemberEdge', 'createdAt');
    }
    if (cursor == null) {
      throw new BuiltValueNullFieldError('EventChannelMemberEdge', 'cursor');
    }
  }

  @override
  EventChannelMemberEdge rebuild(
          void updates(EventChannelMemberEdgeBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  EventChannelMemberEdgeBuilder toBuilder() =>
      new EventChannelMemberEdgeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EventChannelMemberEdge &&
        node == other.node &&
        state == other.state &&
        createdAt == other.createdAt &&
        cursor == other.cursor;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, node.hashCode), state.hashCode), createdAt.hashCode),
        cursor.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('EventChannelMemberEdge')
          ..add('node', node)
          ..add('state', state)
          ..add('createdAt', createdAt)
          ..add('cursor', cursor))
        .toString();
  }
}

class EventChannelMemberEdgeBuilder
    implements Builder<EventChannelMemberEdge, EventChannelMemberEdgeBuilder> {
  _$EventChannelMemberEdge _$v;

  Actor _node;
  Actor get node => _$this._node;
  set node(Actor node) => _$this._node = node;

  UserEventState _state;
  UserEventState get state => _$this._state;
  set state(UserEventState state) => _$this._state = state;

  DateTime _createdAt;
  DateTime get createdAt => _$this._createdAt;
  set createdAt(DateTime createdAt) => _$this._createdAt = createdAt;

  String _cursor;
  String get cursor => _$this._cursor;
  set cursor(String cursor) => _$this._cursor = cursor;

  EventChannelMemberEdgeBuilder();

  EventChannelMemberEdgeBuilder get _$this {
    if (_$v != null) {
      _node = _$v.node;
      _state = _$v.state;
      _createdAt = _$v.createdAt;
      _cursor = _$v.cursor;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EventChannelMemberEdge other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$EventChannelMemberEdge;
  }

  @override
  void update(void updates(EventChannelMemberEdgeBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$EventChannelMemberEdge build() {
    final _$result = _$v ??
        new _$EventChannelMemberEdge._(
            node: node, state: state, createdAt: createdAt, cursor: cursor);
    replace(_$result);
    return _$result;
  }
}

class _$EventChannelMessage extends EventChannelMessage {
  @override
  final Channel channel;
  @override
  final Actor author;
  @override
  final String body;
  @override
  final ChannelMessageState state;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;
  @override
  final String id;

  factory _$EventChannelMessage([void updates(EventChannelMessageBuilder b)]) =>
      (new EventChannelMessageBuilder()..update(updates)).build();

  _$EventChannelMessage._(
      {this.channel,
      this.author,
      this.body,
      this.state,
      this.createdAt,
      this.updatedAt,
      this.id})
      : super._() {
    if (channel == null) {
      throw new BuiltValueNullFieldError('EventChannelMessage', 'channel');
    }
    if (author == null) {
      throw new BuiltValueNullFieldError('EventChannelMessage', 'author');
    }
    if (body == null) {
      throw new BuiltValueNullFieldError('EventChannelMessage', 'body');
    }
    if (state == null) {
      throw new BuiltValueNullFieldError('EventChannelMessage', 'state');
    }
    if (createdAt == null) {
      throw new BuiltValueNullFieldError('EventChannelMessage', 'createdAt');
    }
    if (updatedAt == null) {
      throw new BuiltValueNullFieldError('EventChannelMessage', 'updatedAt');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('EventChannelMessage', 'id');
    }
  }

  @override
  EventChannelMessage rebuild(void updates(EventChannelMessageBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  EventChannelMessageBuilder toBuilder() =>
      new EventChannelMessageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EventChannelMessage &&
        channel == other.channel &&
        author == other.author &&
        body == other.body &&
        state == other.state &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt &&
        id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, channel.hashCode), author.hashCode),
                        body.hashCode),
                    state.hashCode),
                createdAt.hashCode),
            updatedAt.hashCode),
        id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('EventChannelMessage')
          ..add('channel', channel)
          ..add('author', author)
          ..add('body', body)
          ..add('state', state)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt)
          ..add('id', id))
        .toString();
  }
}

class EventChannelMessageBuilder
    implements Builder<EventChannelMessage, EventChannelMessageBuilder> {
  _$EventChannelMessage _$v;

  Channel _channel;
  Channel get channel => _$this._channel;
  set channel(Channel channel) => _$this._channel = channel;

  Actor _author;
  Actor get author => _$this._author;
  set author(Actor author) => _$this._author = author;

  String _body;
  String get body => _$this._body;
  set body(String body) => _$this._body = body;

  ChannelMessageState _state;
  ChannelMessageState get state => _$this._state;
  set state(ChannelMessageState state) => _$this._state = state;

  DateTime _createdAt;
  DateTime get createdAt => _$this._createdAt;
  set createdAt(DateTime createdAt) => _$this._createdAt = createdAt;

  DateTime _updatedAt;
  DateTime get updatedAt => _$this._updatedAt;
  set updatedAt(DateTime updatedAt) => _$this._updatedAt = updatedAt;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  EventChannelMessageBuilder();

  EventChannelMessageBuilder get _$this {
    if (_$v != null) {
      _channel = _$v.channel;
      _author = _$v.author;
      _body = _$v.body;
      _state = _$v.state;
      _createdAt = _$v.createdAt;
      _updatedAt = _$v.updatedAt;
      _id = _$v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EventChannelMessage other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$EventChannelMessage;
  }

  @override
  void update(void updates(EventChannelMessageBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$EventChannelMessage build() {
    final _$result = _$v ??
        new _$EventChannelMessage._(
            channel: channel,
            author: author,
            body: body,
            state: state,
            createdAt: createdAt,
            updatedAt: updatedAt,
            id: id);
    replace(_$result);
    return _$result;
  }
}

class _$EventChannelMessageConnection extends EventChannelMessageConnection {
  @override
  final BuiltList<EventChannelMessageEdge> edges;
  @override
  final BuiltList<EventChannelMessage> nodes;
  @override
  final PageInfo pageInfo;
  @override
  final int count;

  factory _$EventChannelMessageConnection(
          [void updates(EventChannelMessageConnectionBuilder b)]) =>
      (new EventChannelMessageConnectionBuilder()..update(updates)).build();

  _$EventChannelMessageConnection._(
      {this.edges, this.nodes, this.pageInfo, this.count})
      : super._() {
    if (edges == null) {
      throw new BuiltValueNullFieldError(
          'EventChannelMessageConnection', 'edges');
    }
    if (nodes == null) {
      throw new BuiltValueNullFieldError(
          'EventChannelMessageConnection', 'nodes');
    }
    if (pageInfo == null) {
      throw new BuiltValueNullFieldError(
          'EventChannelMessageConnection', 'pageInfo');
    }
    if (count == null) {
      throw new BuiltValueNullFieldError(
          'EventChannelMessageConnection', 'count');
    }
  }

  @override
  EventChannelMessageConnection rebuild(
          void updates(EventChannelMessageConnectionBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  EventChannelMessageConnectionBuilder toBuilder() =>
      new EventChannelMessageConnectionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EventChannelMessageConnection &&
        edges == other.edges &&
        nodes == other.nodes &&
        pageInfo == other.pageInfo &&
        count == other.count;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, edges.hashCode), nodes.hashCode), pageInfo.hashCode),
        count.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('EventChannelMessageConnection')
          ..add('edges', edges)
          ..add('nodes', nodes)
          ..add('pageInfo', pageInfo)
          ..add('count', count))
        .toString();
  }
}

class EventChannelMessageConnectionBuilder
    implements
        Builder<EventChannelMessageConnection,
            EventChannelMessageConnectionBuilder> {
  _$EventChannelMessageConnection _$v;

  ListBuilder<EventChannelMessageEdge> _edges;
  ListBuilder<EventChannelMessageEdge> get edges =>
      _$this._edges ??= new ListBuilder<EventChannelMessageEdge>();
  set edges(ListBuilder<EventChannelMessageEdge> edges) =>
      _$this._edges = edges;

  ListBuilder<EventChannelMessage> _nodes;
  ListBuilder<EventChannelMessage> get nodes =>
      _$this._nodes ??= new ListBuilder<EventChannelMessage>();
  set nodes(ListBuilder<EventChannelMessage> nodes) => _$this._nodes = nodes;

  PageInfoBuilder _pageInfo;
  PageInfoBuilder get pageInfo => _$this._pageInfo ??= new PageInfoBuilder();
  set pageInfo(PageInfoBuilder pageInfo) => _$this._pageInfo = pageInfo;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  EventChannelMessageConnectionBuilder();

  EventChannelMessageConnectionBuilder get _$this {
    if (_$v != null) {
      _edges = _$v.edges?.toBuilder();
      _nodes = _$v.nodes?.toBuilder();
      _pageInfo = _$v.pageInfo?.toBuilder();
      _count = _$v.count;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EventChannelMessageConnection other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$EventChannelMessageConnection;
  }

  @override
  void update(void updates(EventChannelMessageConnectionBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$EventChannelMessageConnection build() {
    _$EventChannelMessageConnection _$result;
    try {
      _$result = _$v ??
          new _$EventChannelMessageConnection._(
              edges: edges.build(),
              nodes: nodes.build(),
              pageInfo: pageInfo.build(),
              count: count);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'edges';
        edges.build();
        _$failedField = 'nodes';
        nodes.build();
        _$failedField = 'pageInfo';
        pageInfo.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'EventChannelMessageConnection', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$EventChannelMessageEdge extends EventChannelMessageEdge {
  @override
  final EventChannelMessage node;
  @override
  final String cursor;

  factory _$EventChannelMessageEdge(
          [void updates(EventChannelMessageEdgeBuilder b)]) =>
      (new EventChannelMessageEdgeBuilder()..update(updates)).build();

  _$EventChannelMessageEdge._({this.node, this.cursor}) : super._() {
    if (node == null) {
      throw new BuiltValueNullFieldError('EventChannelMessageEdge', 'node');
    }
    if (cursor == null) {
      throw new BuiltValueNullFieldError('EventChannelMessageEdge', 'cursor');
    }
  }

  @override
  EventChannelMessageEdge rebuild(
          void updates(EventChannelMessageEdgeBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  EventChannelMessageEdgeBuilder toBuilder() =>
      new EventChannelMessageEdgeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EventChannelMessageEdge &&
        node == other.node &&
        cursor == other.cursor;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, node.hashCode), cursor.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('EventChannelMessageEdge')
          ..add('node', node)
          ..add('cursor', cursor))
        .toString();
  }
}

class EventChannelMessageEdgeBuilder
    implements
        Builder<EventChannelMessageEdge, EventChannelMessageEdgeBuilder> {
  _$EventChannelMessageEdge _$v;

  EventChannelMessageBuilder _node;
  EventChannelMessageBuilder get node =>
      _$this._node ??= new EventChannelMessageBuilder();
  set node(EventChannelMessageBuilder node) => _$this._node = node;

  String _cursor;
  String get cursor => _$this._cursor;
  set cursor(String cursor) => _$this._cursor = cursor;

  EventChannelMessageEdgeBuilder();

  EventChannelMessageEdgeBuilder get _$this {
    if (_$v != null) {
      _node = _$v.node?.toBuilder();
      _cursor = _$v.cursor;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EventChannelMessageEdge other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$EventChannelMessageEdge;
  }

  @override
  void update(void updates(EventChannelMessageEdgeBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$EventChannelMessageEdge build() {
    _$EventChannelMessageEdge _$result;
    try {
      _$result = _$v ??
          new _$EventChannelMessageEdge._(node: node.build(), cursor: cursor);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'node';
        node.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'EventChannelMessageEdge', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$PersonalChannel extends PersonalChannel {
  @override
  final String name;
  @override
  final DateTime viewerLastReadAt;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;
  @override
  final String id;

  factory _$PersonalChannel([void updates(PersonalChannelBuilder b)]) =>
      (new PersonalChannelBuilder()..update(updates)).build();

  _$PersonalChannel._(
      {this.name,
      this.viewerLastReadAt,
      this.createdAt,
      this.updatedAt,
      this.id})
      : super._() {
    if (name == null) {
      throw new BuiltValueNullFieldError('PersonalChannel', 'name');
    }
    if (viewerLastReadAt == null) {
      throw new BuiltValueNullFieldError('PersonalChannel', 'viewerLastReadAt');
    }
    if (createdAt == null) {
      throw new BuiltValueNullFieldError('PersonalChannel', 'createdAt');
    }
    if (updatedAt == null) {
      throw new BuiltValueNullFieldError('PersonalChannel', 'updatedAt');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('PersonalChannel', 'id');
    }
  }

  @override
  PersonalChannel rebuild(void updates(PersonalChannelBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  PersonalChannelBuilder toBuilder() =>
      new PersonalChannelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PersonalChannel &&
        name == other.name &&
        viewerLastReadAt == other.viewerLastReadAt &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt &&
        id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, name.hashCode), viewerLastReadAt.hashCode),
                createdAt.hashCode),
            updatedAt.hashCode),
        id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PersonalChannel')
          ..add('name', name)
          ..add('viewerLastReadAt', viewerLastReadAt)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt)
          ..add('id', id))
        .toString();
  }
}

class PersonalChannelBuilder
    implements Builder<PersonalChannel, PersonalChannelBuilder> {
  _$PersonalChannel _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  DateTime _viewerLastReadAt;
  DateTime get viewerLastReadAt => _$this._viewerLastReadAt;
  set viewerLastReadAt(DateTime viewerLastReadAt) =>
      _$this._viewerLastReadAt = viewerLastReadAt;

  DateTime _createdAt;
  DateTime get createdAt => _$this._createdAt;
  set createdAt(DateTime createdAt) => _$this._createdAt = createdAt;

  DateTime _updatedAt;
  DateTime get updatedAt => _$this._updatedAt;
  set updatedAt(DateTime updatedAt) => _$this._updatedAt = updatedAt;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  PersonalChannelBuilder();

  PersonalChannelBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _viewerLastReadAt = _$v.viewerLastReadAt;
      _createdAt = _$v.createdAt;
      _updatedAt = _$v.updatedAt;
      _id = _$v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PersonalChannel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PersonalChannel;
  }

  @override
  void update(void updates(PersonalChannelBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$PersonalChannel build() {
    final _$result = _$v ??
        new _$PersonalChannel._(
            name: name,
            viewerLastReadAt: viewerLastReadAt,
            createdAt: createdAt,
            updatedAt: updatedAt,
            id: id);
    replace(_$result);
    return _$result;
  }
}

class _$PersonalChannelConnection extends PersonalChannelConnection {
  @override
  final BuiltList<PersonalChannelEdge> edges;
  @override
  final BuiltList<PersonalChannel> nodes;
  @override
  final PageInfo pageInfo;
  @override
  final int count;

  factory _$PersonalChannelConnection(
          [void updates(PersonalChannelConnectionBuilder b)]) =>
      (new PersonalChannelConnectionBuilder()..update(updates)).build();

  _$PersonalChannelConnection._(
      {this.edges, this.nodes, this.pageInfo, this.count})
      : super._() {
    if (edges == null) {
      throw new BuiltValueNullFieldError('PersonalChannelConnection', 'edges');
    }
    if (nodes == null) {
      throw new BuiltValueNullFieldError('PersonalChannelConnection', 'nodes');
    }
    if (pageInfo == null) {
      throw new BuiltValueNullFieldError(
          'PersonalChannelConnection', 'pageInfo');
    }
    if (count == null) {
      throw new BuiltValueNullFieldError('PersonalChannelConnection', 'count');
    }
  }

  @override
  PersonalChannelConnection rebuild(
          void updates(PersonalChannelConnectionBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  PersonalChannelConnectionBuilder toBuilder() =>
      new PersonalChannelConnectionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PersonalChannelConnection &&
        edges == other.edges &&
        nodes == other.nodes &&
        pageInfo == other.pageInfo &&
        count == other.count;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, edges.hashCode), nodes.hashCode), pageInfo.hashCode),
        count.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PersonalChannelConnection')
          ..add('edges', edges)
          ..add('nodes', nodes)
          ..add('pageInfo', pageInfo)
          ..add('count', count))
        .toString();
  }
}

class PersonalChannelConnectionBuilder
    implements
        Builder<PersonalChannelConnection, PersonalChannelConnectionBuilder> {
  _$PersonalChannelConnection _$v;

  ListBuilder<PersonalChannelEdge> _edges;
  ListBuilder<PersonalChannelEdge> get edges =>
      _$this._edges ??= new ListBuilder<PersonalChannelEdge>();
  set edges(ListBuilder<PersonalChannelEdge> edges) => _$this._edges = edges;

  ListBuilder<PersonalChannel> _nodes;
  ListBuilder<PersonalChannel> get nodes =>
      _$this._nodes ??= new ListBuilder<PersonalChannel>();
  set nodes(ListBuilder<PersonalChannel> nodes) => _$this._nodes = nodes;

  PageInfoBuilder _pageInfo;
  PageInfoBuilder get pageInfo => _$this._pageInfo ??= new PageInfoBuilder();
  set pageInfo(PageInfoBuilder pageInfo) => _$this._pageInfo = pageInfo;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  PersonalChannelConnectionBuilder();

  PersonalChannelConnectionBuilder get _$this {
    if (_$v != null) {
      _edges = _$v.edges?.toBuilder();
      _nodes = _$v.nodes?.toBuilder();
      _pageInfo = _$v.pageInfo?.toBuilder();
      _count = _$v.count;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PersonalChannelConnection other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PersonalChannelConnection;
  }

  @override
  void update(void updates(PersonalChannelConnectionBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$PersonalChannelConnection build() {
    _$PersonalChannelConnection _$result;
    try {
      _$result = _$v ??
          new _$PersonalChannelConnection._(
              edges: edges.build(),
              nodes: nodes.build(),
              pageInfo: pageInfo.build(),
              count: count);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'edges';
        edges.build();
        _$failedField = 'nodes';
        nodes.build();
        _$failedField = 'pageInfo';
        pageInfo.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PersonalChannelConnection', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$PersonalChannelEdge extends PersonalChannelEdge {
  @override
  final PersonalChannel node;
  @override
  final String cursor;

  factory _$PersonalChannelEdge([void updates(PersonalChannelEdgeBuilder b)]) =>
      (new PersonalChannelEdgeBuilder()..update(updates)).build();

  _$PersonalChannelEdge._({this.node, this.cursor}) : super._() {
    if (node == null) {
      throw new BuiltValueNullFieldError('PersonalChannelEdge', 'node');
    }
    if (cursor == null) {
      throw new BuiltValueNullFieldError('PersonalChannelEdge', 'cursor');
    }
  }

  @override
  PersonalChannelEdge rebuild(void updates(PersonalChannelEdgeBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  PersonalChannelEdgeBuilder toBuilder() =>
      new PersonalChannelEdgeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PersonalChannelEdge &&
        node == other.node &&
        cursor == other.cursor;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, node.hashCode), cursor.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PersonalChannelEdge')
          ..add('node', node)
          ..add('cursor', cursor))
        .toString();
  }
}

class PersonalChannelEdgeBuilder
    implements Builder<PersonalChannelEdge, PersonalChannelEdgeBuilder> {
  _$PersonalChannelEdge _$v;

  PersonalChannelBuilder _node;
  PersonalChannelBuilder get node =>
      _$this._node ??= new PersonalChannelBuilder();
  set node(PersonalChannelBuilder node) => _$this._node = node;

  String _cursor;
  String get cursor => _$this._cursor;
  set cursor(String cursor) => _$this._cursor = cursor;

  PersonalChannelEdgeBuilder();

  PersonalChannelEdgeBuilder get _$this {
    if (_$v != null) {
      _node = _$v.node?.toBuilder();
      _cursor = _$v.cursor;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PersonalChannelEdge other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PersonalChannelEdge;
  }

  @override
  void update(void updates(PersonalChannelEdgeBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$PersonalChannelEdge build() {
    _$PersonalChannelEdge _$result;
    try {
      _$result = _$v ??
          new _$PersonalChannelEdge._(node: node.build(), cursor: cursor);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'node';
        node.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PersonalChannelEdge', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$PersonalChannelMessage extends PersonalChannelMessage {
  @override
  final String id;
  @override
  final Channel channel;
  @override
  final Actor author;
  @override
  final String body;
  @override
  final ChannelMessageState state;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;

  factory _$PersonalChannelMessage(
          [void updates(PersonalChannelMessageBuilder b)]) =>
      (new PersonalChannelMessageBuilder()..update(updates)).build();

  _$PersonalChannelMessage._(
      {this.id,
      this.channel,
      this.author,
      this.body,
      this.state,
      this.createdAt,
      this.updatedAt})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('PersonalChannelMessage', 'id');
    }
    if (channel == null) {
      throw new BuiltValueNullFieldError('PersonalChannelMessage', 'channel');
    }
    if (author == null) {
      throw new BuiltValueNullFieldError('PersonalChannelMessage', 'author');
    }
    if (body == null) {
      throw new BuiltValueNullFieldError('PersonalChannelMessage', 'body');
    }
    if (state == null) {
      throw new BuiltValueNullFieldError('PersonalChannelMessage', 'state');
    }
    if (createdAt == null) {
      throw new BuiltValueNullFieldError('PersonalChannelMessage', 'createdAt');
    }
    if (updatedAt == null) {
      throw new BuiltValueNullFieldError('PersonalChannelMessage', 'updatedAt');
    }
  }

  @override
  PersonalChannelMessage rebuild(
          void updates(PersonalChannelMessageBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  PersonalChannelMessageBuilder toBuilder() =>
      new PersonalChannelMessageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PersonalChannelMessage &&
        id == other.id &&
        channel == other.channel &&
        author == other.author &&
        body == other.body &&
        state == other.state &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, id.hashCode), channel.hashCode),
                        author.hashCode),
                    body.hashCode),
                state.hashCode),
            createdAt.hashCode),
        updatedAt.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PersonalChannelMessage')
          ..add('id', id)
          ..add('channel', channel)
          ..add('author', author)
          ..add('body', body)
          ..add('state', state)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt))
        .toString();
  }
}

class PersonalChannelMessageBuilder
    implements Builder<PersonalChannelMessage, PersonalChannelMessageBuilder> {
  _$PersonalChannelMessage _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  Channel _channel;
  Channel get channel => _$this._channel;
  set channel(Channel channel) => _$this._channel = channel;

  Actor _author;
  Actor get author => _$this._author;
  set author(Actor author) => _$this._author = author;

  String _body;
  String get body => _$this._body;
  set body(String body) => _$this._body = body;

  ChannelMessageState _state;
  ChannelMessageState get state => _$this._state;
  set state(ChannelMessageState state) => _$this._state = state;

  DateTime _createdAt;
  DateTime get createdAt => _$this._createdAt;
  set createdAt(DateTime createdAt) => _$this._createdAt = createdAt;

  DateTime _updatedAt;
  DateTime get updatedAt => _$this._updatedAt;
  set updatedAt(DateTime updatedAt) => _$this._updatedAt = updatedAt;

  PersonalChannelMessageBuilder();

  PersonalChannelMessageBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _channel = _$v.channel;
      _author = _$v.author;
      _body = _$v.body;
      _state = _$v.state;
      _createdAt = _$v.createdAt;
      _updatedAt = _$v.updatedAt;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PersonalChannelMessage other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PersonalChannelMessage;
  }

  @override
  void update(void updates(PersonalChannelMessageBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$PersonalChannelMessage build() {
    final _$result = _$v ??
        new _$PersonalChannelMessage._(
            id: id,
            channel: channel,
            author: author,
            body: body,
            state: state,
            createdAt: createdAt,
            updatedAt: updatedAt);
    replace(_$result);
    return _$result;
  }
}

class _$PersonalChannelMessageConnection
    extends PersonalChannelMessageConnection {
  @override
  final BuiltList<PersonalChannelMessageEdge> edges;
  @override
  final BuiltList<PersonalChannelMessage> nodes;
  @override
  final ChannelMessage node;
  @override
  final String cursor;

  factory _$PersonalChannelMessageConnection(
          [void updates(PersonalChannelMessageConnectionBuilder b)]) =>
      (new PersonalChannelMessageConnectionBuilder()..update(updates)).build();

  _$PersonalChannelMessageConnection._(
      {this.edges, this.nodes, this.node, this.cursor})
      : super._() {
    if (edges == null) {
      throw new BuiltValueNullFieldError(
          'PersonalChannelMessageConnection', 'edges');
    }
    if (nodes == null) {
      throw new BuiltValueNullFieldError(
          'PersonalChannelMessageConnection', 'nodes');
    }
    if (node == null) {
      throw new BuiltValueNullFieldError(
          'PersonalChannelMessageConnection', 'node');
    }
    if (cursor == null) {
      throw new BuiltValueNullFieldError(
          'PersonalChannelMessageConnection', 'cursor');
    }
  }

  @override
  PersonalChannelMessageConnection rebuild(
          void updates(PersonalChannelMessageConnectionBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  PersonalChannelMessageConnectionBuilder toBuilder() =>
      new PersonalChannelMessageConnectionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PersonalChannelMessageConnection &&
        edges == other.edges &&
        nodes == other.nodes &&
        node == other.node &&
        cursor == other.cursor;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, edges.hashCode), nodes.hashCode), node.hashCode),
        cursor.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PersonalChannelMessageConnection')
          ..add('edges', edges)
          ..add('nodes', nodes)
          ..add('node', node)
          ..add('cursor', cursor))
        .toString();
  }
}

class PersonalChannelMessageConnectionBuilder
    implements
        Builder<PersonalChannelMessageConnection,
            PersonalChannelMessageConnectionBuilder> {
  _$PersonalChannelMessageConnection _$v;

  ListBuilder<PersonalChannelMessageEdge> _edges;
  ListBuilder<PersonalChannelMessageEdge> get edges =>
      _$this._edges ??= new ListBuilder<PersonalChannelMessageEdge>();
  set edges(ListBuilder<PersonalChannelMessageEdge> edges) =>
      _$this._edges = edges;

  ListBuilder<PersonalChannelMessage> _nodes;
  ListBuilder<PersonalChannelMessage> get nodes =>
      _$this._nodes ??= new ListBuilder<PersonalChannelMessage>();
  set nodes(ListBuilder<PersonalChannelMessage> nodes) => _$this._nodes = nodes;

  ChannelMessage _node;
  ChannelMessage get node => _$this._node;
  set node(ChannelMessage node) => _$this._node = node;

  String _cursor;
  String get cursor => _$this._cursor;
  set cursor(String cursor) => _$this._cursor = cursor;

  PersonalChannelMessageConnectionBuilder();

  PersonalChannelMessageConnectionBuilder get _$this {
    if (_$v != null) {
      _edges = _$v.edges?.toBuilder();
      _nodes = _$v.nodes?.toBuilder();
      _node = _$v.node;
      _cursor = _$v.cursor;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PersonalChannelMessageConnection other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PersonalChannelMessageConnection;
  }

  @override
  void update(void updates(PersonalChannelMessageConnectionBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$PersonalChannelMessageConnection build() {
    _$PersonalChannelMessageConnection _$result;
    try {
      _$result = _$v ??
          new _$PersonalChannelMessageConnection._(
              edges: edges.build(),
              nodes: nodes.build(),
              node: node,
              cursor: cursor);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'edges';
        edges.build();
        _$failedField = 'nodes';
        nodes.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PersonalChannelMessageConnection', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$PersonalChannelMessageEdge extends PersonalChannelMessageEdge {
  @override
  final PersonalChannelMessage node;
  @override
  final String cursor;

  factory _$PersonalChannelMessageEdge(
          [void updates(PersonalChannelMessageEdgeBuilder b)]) =>
      (new PersonalChannelMessageEdgeBuilder()..update(updates)).build();

  _$PersonalChannelMessageEdge._({this.node, this.cursor}) : super._() {
    if (node == null) {
      throw new BuiltValueNullFieldError('PersonalChannelMessageEdge', 'node');
    }
    if (cursor == null) {
      throw new BuiltValueNullFieldError(
          'PersonalChannelMessageEdge', 'cursor');
    }
  }

  @override
  PersonalChannelMessageEdge rebuild(
          void updates(PersonalChannelMessageEdgeBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  PersonalChannelMessageEdgeBuilder toBuilder() =>
      new PersonalChannelMessageEdgeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PersonalChannelMessageEdge &&
        node == other.node &&
        cursor == other.cursor;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, node.hashCode), cursor.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PersonalChannelMessageEdge')
          ..add('node', node)
          ..add('cursor', cursor))
        .toString();
  }
}

class PersonalChannelMessageEdgeBuilder
    implements
        Builder<PersonalChannelMessageEdge, PersonalChannelMessageEdgeBuilder> {
  _$PersonalChannelMessageEdge _$v;

  PersonalChannelMessageBuilder _node;
  PersonalChannelMessageBuilder get node =>
      _$this._node ??= new PersonalChannelMessageBuilder();
  set node(PersonalChannelMessageBuilder node) => _$this._node = node;

  String _cursor;
  String get cursor => _$this._cursor;
  set cursor(String cursor) => _$this._cursor = cursor;

  PersonalChannelMessageEdgeBuilder();

  PersonalChannelMessageEdgeBuilder get _$this {
    if (_$v != null) {
      _node = _$v.node?.toBuilder();
      _cursor = _$v.cursor;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PersonalChannelMessageEdge other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PersonalChannelMessageEdge;
  }

  @override
  void update(void updates(PersonalChannelMessageEdgeBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$PersonalChannelMessageEdge build() {
    _$PersonalChannelMessageEdge _$result;
    try {
      _$result = _$v ??
          new _$PersonalChannelMessageEdge._(
              node: node.build(), cursor: cursor);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'node';
        node.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PersonalChannelMessageEdge', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$PersonalChannelMemberConnection
    extends PersonalChannelMemberConnection {
  @override
  final BuiltList<PersonalChannelMemberEdge> edges;
  @override
  final BuiltList<Actor> nodes;
  @override
  final PageInfo pageInfo;
  @override
  final int count;

  factory _$PersonalChannelMemberConnection(
          [void updates(PersonalChannelMemberConnectionBuilder b)]) =>
      (new PersonalChannelMemberConnectionBuilder()..update(updates)).build();

  _$PersonalChannelMemberConnection._(
      {this.edges, this.nodes, this.pageInfo, this.count})
      : super._() {
    if (edges == null) {
      throw new BuiltValueNullFieldError(
          'PersonalChannelMemberConnection', 'edges');
    }
    if (nodes == null) {
      throw new BuiltValueNullFieldError(
          'PersonalChannelMemberConnection', 'nodes');
    }
    if (pageInfo == null) {
      throw new BuiltValueNullFieldError(
          'PersonalChannelMemberConnection', 'pageInfo');
    }
    if (count == null) {
      throw new BuiltValueNullFieldError(
          'PersonalChannelMemberConnection', 'count');
    }
  }

  @override
  PersonalChannelMemberConnection rebuild(
          void updates(PersonalChannelMemberConnectionBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  PersonalChannelMemberConnectionBuilder toBuilder() =>
      new PersonalChannelMemberConnectionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PersonalChannelMemberConnection &&
        edges == other.edges &&
        nodes == other.nodes &&
        pageInfo == other.pageInfo &&
        count == other.count;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, edges.hashCode), nodes.hashCode), pageInfo.hashCode),
        count.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PersonalChannelMemberConnection')
          ..add('edges', edges)
          ..add('nodes', nodes)
          ..add('pageInfo', pageInfo)
          ..add('count', count))
        .toString();
  }
}

class PersonalChannelMemberConnectionBuilder
    implements
        Builder<PersonalChannelMemberConnection,
            PersonalChannelMemberConnectionBuilder> {
  _$PersonalChannelMemberConnection _$v;

  ListBuilder<PersonalChannelMemberEdge> _edges;
  ListBuilder<PersonalChannelMemberEdge> get edges =>
      _$this._edges ??= new ListBuilder<PersonalChannelMemberEdge>();
  set edges(ListBuilder<PersonalChannelMemberEdge> edges) =>
      _$this._edges = edges;

  ListBuilder<Actor> _nodes;
  ListBuilder<Actor> get nodes => _$this._nodes ??= new ListBuilder<Actor>();
  set nodes(ListBuilder<Actor> nodes) => _$this._nodes = nodes;

  PageInfoBuilder _pageInfo;
  PageInfoBuilder get pageInfo => _$this._pageInfo ??= new PageInfoBuilder();
  set pageInfo(PageInfoBuilder pageInfo) => _$this._pageInfo = pageInfo;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  PersonalChannelMemberConnectionBuilder();

  PersonalChannelMemberConnectionBuilder get _$this {
    if (_$v != null) {
      _edges = _$v.edges?.toBuilder();
      _nodes = _$v.nodes?.toBuilder();
      _pageInfo = _$v.pageInfo?.toBuilder();
      _count = _$v.count;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PersonalChannelMemberConnection other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PersonalChannelMemberConnection;
  }

  @override
  void update(void updates(PersonalChannelMemberConnectionBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$PersonalChannelMemberConnection build() {
    _$PersonalChannelMemberConnection _$result;
    try {
      _$result = _$v ??
          new _$PersonalChannelMemberConnection._(
              edges: edges.build(),
              nodes: nodes.build(),
              pageInfo: pageInfo.build(),
              count: count);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'edges';
        edges.build();
        _$failedField = 'nodes';
        nodes.build();
        _$failedField = 'pageInfo';
        pageInfo.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PersonalChannelMemberConnection', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$PersonalChannelMemberEdge extends PersonalChannelMemberEdge {
  @override
  final Actor node;
  @override
  final DateTime createdAt;
  @override
  final String cursor;

  factory _$PersonalChannelMemberEdge(
          [void updates(PersonalChannelMemberEdgeBuilder b)]) =>
      (new PersonalChannelMemberEdgeBuilder()..update(updates)).build();

  _$PersonalChannelMemberEdge._({this.node, this.createdAt, this.cursor})
      : super._() {
    if (node == null) {
      throw new BuiltValueNullFieldError('PersonalChannelMemberEdge', 'node');
    }
    if (createdAt == null) {
      throw new BuiltValueNullFieldError(
          'PersonalChannelMemberEdge', 'createdAt');
    }
    if (cursor == null) {
      throw new BuiltValueNullFieldError('PersonalChannelMemberEdge', 'cursor');
    }
  }

  @override
  PersonalChannelMemberEdge rebuild(
          void updates(PersonalChannelMemberEdgeBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  PersonalChannelMemberEdgeBuilder toBuilder() =>
      new PersonalChannelMemberEdgeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PersonalChannelMemberEdge &&
        node == other.node &&
        createdAt == other.createdAt &&
        cursor == other.cursor;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, node.hashCode), createdAt.hashCode), cursor.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PersonalChannelMemberEdge')
          ..add('node', node)
          ..add('createdAt', createdAt)
          ..add('cursor', cursor))
        .toString();
  }
}

class PersonalChannelMemberEdgeBuilder
    implements
        Builder<PersonalChannelMemberEdge, PersonalChannelMemberEdgeBuilder> {
  _$PersonalChannelMemberEdge _$v;

  Actor _node;
  Actor get node => _$this._node;
  set node(Actor node) => _$this._node = node;

  DateTime _createdAt;
  DateTime get createdAt => _$this._createdAt;
  set createdAt(DateTime createdAt) => _$this._createdAt = createdAt;

  String _cursor;
  String get cursor => _$this._cursor;
  set cursor(String cursor) => _$this._cursor = cursor;

  PersonalChannelMemberEdgeBuilder();

  PersonalChannelMemberEdgeBuilder get _$this {
    if (_$v != null) {
      _node = _$v.node;
      _createdAt = _$v.createdAt;
      _cursor = _$v.cursor;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PersonalChannelMemberEdge other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PersonalChannelMemberEdge;
  }

  @override
  void update(void updates(PersonalChannelMemberEdgeBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$PersonalChannelMemberEdge build() {
    final _$result = _$v ??
        new _$PersonalChannelMemberEdge._(
            node: node, createdAt: createdAt, cursor: cursor);
    replace(_$result);
    return _$result;
  }
}
